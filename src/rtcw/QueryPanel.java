

package rtcw;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.border.*;
import java.awt.*;
import javax.swing.Box;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import javax.swing.ImageIcon;

/* RadioButton needs ButtonGroup */
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;




public class QueryPanel extends JPanel implements ActionListener
{

		// Limit selection to 120 rounds - to avoid drawing crash
		static int ROUND_HARD_CODED_LIMIT = 999;
		
		// General
		ImageIcon	logoIcon	   = ETStat.createImageIcon("MiscIconPack/logo214x102.png");

		// Skills
		ImageIcon	bSenseIcon		= ETStat.createImageIcon("SkillsIconPack/bsense32.png");		
	  	ImageIcon   bSenseIcon2     = ETStat.createImageIcon("SkillsIconPack/bsense32s2.png");
		ImageIcon	lWeaponIcon		= ETStat.createImageIcon("SkillsIconPack/lweapon32.png");		
		ImageIcon	lWeaponIcon2	= ETStat.createImageIcon("SkillsIconPack/lweapon32s2.png");		
		ImageIcon	hWeaponIcon		= ETStat.createImageIcon("SkillsIconPack/hweapon32.png");		
		ImageIcon	hWeaponIcon2	= ETStat.createImageIcon("SkillsIconPack/hweapon32s2.png");		
		ImageIcon	covOpsIcon		= ETStat.createImageIcon("SkillsIconPack/covops32.png");		
		ImageIcon	covOpsIcon2		= ETStat.createImageIcon("SkillsIconPack/covops32s2.png");		
		ImageIcon	engineerIcon	= ETStat.createImageIcon("SkillsIconPack/engineer32.png");		
		ImageIcon	engineerIcon2	= ETStat.createImageIcon("SkillsIconPack/engineer32s2.png");		
		ImageIcon	signalsIcon		= ETStat.createImageIcon("SkillsIconPack/signals32.png");		
		ImageIcon	signalsIcon2	= ETStat.createImageIcon("SkillsIconPack/signals32s2.png");		
		ImageIcon	medicIcon		= ETStat.createImageIcon("SkillsIconPack/medic32.png");		
		ImageIcon	medicIcon2		= ETStat.createImageIcon("SkillsIconPack/medic32s2.png");		

		// Little Weapon
		ImageIcon	lugerIcon		= ETStat.createImageIcon("WeaponsIconPack/L/luger32.png");	
		ImageIcon	lugerIcon2		= ETStat.createImageIcon("WeaponsIconPack/L/luger32s.png");	
		ImageIcon	coltIcon		= ETStat.createImageIcon("WeaponsIconPack/L/colt32.png");	
		ImageIcon	coltIcon2		= ETStat.createImageIcon("WeaponsIconPack/L/colt32s.png");	
		ImageIcon	knifeIcon		= ETStat.createImageIcon("WeaponsIconPack/L/knife32.png");	
		ImageIcon	knifeIcon2		= ETStat.createImageIcon("WeaponsIconPack/L/knife32s.png");	
		ImageIcon	dynamiteIcon	= ETStat.createImageIcon("WeaponsIconPack/L/dynamite32.png");	
		ImageIcon	dynamiteIcon2	= ETStat.createImageIcon("WeaponsIconPack/L/dynamite32s.png");	
		ImageIcon	satchelIcon		= ETStat.createImageIcon("WeaponsIconPack/L/satchel32.png");	
		ImageIcon	satchelIcon2	= ETStat.createImageIcon("WeaponsIconPack/L/satchel32s.png");	
		ImageIcon	binocularsIcon	= ETStat.createImageIcon("WeaponsIconPack/L/binoculars32.png");	
		ImageIcon	binocularsIcon2	= ETStat.createImageIcon("WeaponsIconPack/L/binoculars32s.png");	
		ImageIcon	grenadeIcon		= ETStat.createImageIcon("WeaponsIconPack/L/allied_grenade32.png");	
		ImageIcon	grenadeIcon2	= ETStat.createImageIcon("WeaponsIconPack/L/allied_grenade32s.png");	
		ImageIcon	landMineIcon	= ETStat.createImageIcon("WeaponsIconPack/L/land_mine32.png");	
		ImageIcon	landMineIcon2	= ETStat.createImageIcon("WeaponsIconPack/L/land_mine32s.png");	
		ImageIcon	smokeGrenIcon	= ETStat.createImageIcon("WeaponsIconPack/L/smoke_grenade32.png");	
		ImageIcon	smokeGrenIcon2	= ETStat.createImageIcon("WeaponsIconPack/L/smoke_grenade32s.png");	

		// Big Weapon
		ImageIcon	mp40Icon			= ETStat.createImageIcon("WeaponsIconPack/B/mp40-32.png");	
		ImageIcon	mp40Icon2			= ETStat.createImageIcon("WeaponsIconPack/B/mp40-32s.png");	
		ImageIcon	thompsonIcon		= ETStat.createImageIcon("WeaponsIconPack/B/thompson32.png");	
		ImageIcon	thompsonIcon2		= ETStat.createImageIcon("WeaponsIconPack/B/thompson32s.png");	
		ImageIcon	panzerfaustIcon		= ETStat.createImageIcon("WeaponsIconPack/B/panzerfaust32.png");	
		ImageIcon	panzerfaustIcon2	= ETStat.createImageIcon("WeaponsIconPack/B/panzerfaust32s.png");	
		ImageIcon	mg42Icon			= ETStat.createImageIcon("WeaponsIconPack/B/mg42-32.png");	
		ImageIcon	mg42Icon2			= ETStat.createImageIcon("WeaponsIconPack/B/mg42-32s.png");	
		ImageIcon	mortarIcon			= ETStat.createImageIcon("WeaponsIconPack/B/mortar32.png");	
		ImageIcon	mortarIcon2			= ETStat.createImageIcon("WeaponsIconPack/B/mortar32s.png");	
		ImageIcon	flamerIcon			= ETStat.createImageIcon("WeaponsIconPack/B/flamer32.png");	
		ImageIcon	flamerIcon2			= ETStat.createImageIcon("WeaponsIconPack/B/flamer32s.png");	
		ImageIcon	stenIcon			= ETStat.createImageIcon("WeaponsIconPack/B/sten32.png");	
		ImageIcon	stenIcon2			= ETStat.createImageIcon("WeaponsIconPack/B/sten32s.png");	
		ImageIcon	fg42Icon			= ETStat.createImageIcon("WeaponsIconPack/B/fg42-32.png");	
		ImageIcon	fg42Icon2			= ETStat.createImageIcon("WeaponsIconPack/B/fg42-32s.png");	
		ImageIcon	gLauncherIcon		= ETStat.createImageIcon("WeaponsIconPack/B/k43_and_gpg40_granatwerfer32.png");	
		ImageIcon	gLauncherIcon2		= ETStat.createImageIcon("WeaponsIconPack/B/k43_and_gpg40_granatwerfer32s.png");	
		ImageIcon	garandIcon			= ETStat.createImageIcon("WeaponsIconPack/B/mg1_garand_silenced32.png");	
		ImageIcon	garandIcon2			= ETStat.createImageIcon("WeaponsIconPack/B/mg1_garand_silenced32s.png");	
		ImageIcon	k43Icon				= ETStat.createImageIcon("WeaponsIconPack/B/k43_silenced32.png");	
		ImageIcon	k43Icon2			= ETStat.createImageIcon("WeaponsIconPack/B/k43_silenced32s.png");	
		

		
		ActionManager am;
		MsgOutput o = new MsgOutput( MsgOutput.INFO, "RTCW.QueryPanel" );

		BoxLayout layout =  new BoxLayout( this, BoxLayout.Y_AXIS );
		
		Color titleColor = Color.blue;

		JPanel panelUp =		new JPanel(new GridLayout(1,3));
		JPanel datePanel =		new JPanel(new GridLayout(2,2));
		JPanel dummyPanel =		new JPanel(new GridLayout(3,1,10,2));
		//JPanel dummyPanel =		new JPanel(new FlowLayout());
		//Box dummyPanel =		new Box(BoxLayout.Y_AXIS);

		
		// Lower Panel contains Left Panel and Weapon Pannel
		//JPanel panelDn =			new JPanel(new GridLayout(1,2));//new Box(BoxLayout.X_AXIS);
		//JPanel panelDn =			new JPanel( new FlowLayout());//new Box(BoxLayout.X_AXIS);
		Box panelDn =			new Box(BoxLayout.X_AXIS);//new Box(BoxLayout.X_AXIS);

		
		// Left Panel contains basic Panel and Skills Panel
		Box leftPanel =			new Box(BoxLayout.Y_AXIS);
		//JPanel leftPanel =			new JPanel(new GridLayout(2,1));
		
		// Sub Panels of panelDn
//		Box basicDataPanel =	new Box(BoxLayout.Y_AXIS);
//		Box skillsPanel =		new Box(BoxLayout.Y_AXIS);
		JPanel basicDataPanel = new JPanel( new GridLayout(2,2) );
		//JPanel skillsPanel =	new JPanel( new GridLayout(5,2) );
		//Box skillsPanel =		new Box( BoxLayout.Y_AXIS );
		
		//JPanel skillsPanel =	   new JPanel( new FlowLayout() );
		Box skillsPanel =		   new Box( BoxLayout.X_AXIS );
		JPanel skills0Panel =	   new JPanel( new GridLayout(2,1) );
		JPanel skillsButtonPanel = new JPanel( new GridLayout(2,4) );

		//JPanel skillsPanel =	new JPanel( new FlowLayout() );
		//JPanel skillsPanel =	new JPanel( new GridLayout(2,7) );
		//Box skillsPanel =		new Box( BoxLayout.Y_AXIS );
		

		// Weapon Panel contains weap1Panel and weap2Panel
		Box weaponPanel =		new Box(BoxLayout.X_AXIS);
		//JPanel weaponPanel = new JPanel( );

		// Sub Panels of Weapon Panel
		//Box weap1Panel =		new Box(BoxLayout.Y_AXIS);
		JPanel weap0Panel =		new JPanel(new GridLayout(4,1));
		JPanel weap1Panel =		new JPanel(new GridLayout(4,3));
		//JPanel weap1Panel =		new JPanel(new FlowLayout());
		
		//Box weap2Panel =		new Box(BoxLayout.Y_AXIS);
		JPanel weap2Panel =		new JPanel(new GridLayout(4,3));
		//Box weap3Panel =		new Box(BoxLayout.Y_AXIS);
		//JPanel weap3Panel =		new JPanel(new GridLayout(8,1));

		JTextField startDateTF = new JTextField(Round.get_minDate());
		JTextField endDateTF = new JTextField(Round.get_maxDate());

		JButton showBT = new JButton("Show Graph");
		JButton optionsBT = new JButton("Options");
		JButton overStatsBT = new JButton("Global Stats");
		JButton htmlGenBT  = new JButton("HTML Gen");
		
		Border bevelLowered = BorderFactory.createBevelBorder(BevelBorder.LOWERED);
		Border etchedRaised = BorderFactory.createEtchedBorder(EtchedBorder.RAISED );
		Border lined = BorderFactory.createLineBorder(Color.darkGray);
		//Border bevelLowered = BorderFactory.createBevelBorder(BevelBorder.RAISED);
		Border insideSpaceBorder=	BorderFactory.createEmptyBorder(8,12,8,12);
		
		//Border bevelLowered = BorderFactory.createEmptyBorder();
		Border mo_title1 =	BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(bevelLowered, "Period"), insideSpaceBorder);
		//TitledBorder mo_title2 =	BorderFactory.createTitledBorder(bevelLowered, "Basic data");
		//TitledBorder mo_title3 =	BorderFactory.createTitledBorder(bevelLowered, "Weapons");
		Border mo_title4 =	BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(etchedRaised, "Skills"), insideSpaceBorder);
		Border mo_title3 =	BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(etchedRaised, "Weapons"), insideSpaceBorder);
		Border mo_title2 =	BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(etchedRaised, "Basic Data"), insideSpaceBorder);
		Border dummyBorder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(),  insideSpaceBorder);


		JRadioButton kdRB =		new JRadioButton("kills-deaths", true); 
		JRadioButton hsRB =		new JRadioButton("head shots", false); 
		JRadioButton dgrRB =	new JRadioButton("damage G-R", false);
		JRadioButton dgtRB =	new JRadioButton("damage G-T", false);

		String []  dataTypes= { "Accuracy","Kills","Deaths" };
		JComboBox weapDataTypeCB = new JComboBox ( dataTypes );		
		
		String []  dataTypes2= { "Points","Level","Medals" };
		JComboBox skillDataTypeCB = new JComboBox ( dataTypes2 );		



		JRadioButton lugRB =		new JRadioButton(lugerIcon, false);
		JRadioButton coltRB =		new JRadioButton(coltIcon, false);
		JRadioButton mp40RB =		new JRadioButton(mp40Icon, false);
		JRadioButton thomRB =		new JRadioButton(thompsonIcon, false);
		JRadioButton stenRB =		new JRadioButton(stenIcon, false);
		JRadioButton fg42RB =		new JRadioButton(fg42Icon, false);
		JRadioButton garandRB =		new JRadioButton(garandIcon, false);
		JRadioButton k43RB =		new JRadioButton(k43Icon, false);

		// Sans Headshot
		JRadioButton mg42RB =		new JRadioButton(mg42Icon, false);
		JRadioButton panzerRB =		new JRadioButton(panzerfaustIcon, false);
		JRadioButton fthrowerRB=	new JRadioButton(flamerIcon, false);
		JRadioButton mortarRB =		new JRadioButton(mortarIcon, false);
		

		JRadioButton knifeRB =		new JRadioButton(knifeIcon, false);
		JRadioButton satchelRB =	new JRadioButton(satchelIcon, false);
		JRadioButton grenadeRB =	new JRadioButton(grenadeIcon, false);

		
		JRadioButton grenlaunchRB =	new JRadioButton(gLauncherIcon, false);
		JRadioButton lmineRB =		new JRadioButton(landMineIcon, false);
		JRadioButton dynaRB =		new JRadioButton(dynamiteIcon, false);

		JRadioButton airstrikeRB =	new JRadioButton(smokeGrenIcon, false);
		JRadioButton artilleryRB =	new JRadioButton(binocularsIcon, false);

		// SKILLS	

		JRadioButton bsenseRB=	new JRadioButton( bSenseIcon, false);
		JRadioButton lweaponRB= new JRadioButton( lWeaponIcon, false);
		JRadioButton hweaponRB=	new JRadioButton( hWeaponIcon, false);
		JRadioButton signalsRB=	new JRadioButton( signalsIcon, false);
		JRadioButton covopsRB=	new JRadioButton( covOpsIcon, false);
		JRadioButton engieRB=	new JRadioButton( engineerIcon, false);
		JRadioButton faidRB=	new JRadioButton( medicIcon, false);

		
		ButtonGroup group = new ButtonGroup();
	

	public  QueryPanel( ActionManager actmgr ) {

		this.am = actmgr;	
		this.setLayout( layout );
		showBT.addActionListener(this);
		showBT.setActionCommand("showgraph");
		showBT.setAlignmentX(0.5f);
		optionsBT.addActionListener(this);
		optionsBT.setActionCommand("options");
		optionsBT.setAlignmentX(0.5f);
		overStatsBT.addActionListener(this);		
		overStatsBT.setActionCommand("stats");
		overStatsBT.setAlignmentX(0.5f);
		htmlGenBT.addActionListener(this);		
		htmlGenBT.setActionCommand("html");
		htmlGenBT.setAlignmentX(0.5f);
		

		datePanel.setBorder(mo_title1);
		basicDataPanel.setBorder(mo_title2);
		weaponPanel.setBorder(mo_title3);
		skillsPanel.setBorder(mo_title4);

		datePanel.add ( new JLabel ("Start date"));
		datePanel.add ( startDateTF );
		datePanel.add ( new JLabel ("End date"));
		datePanel.add ( endDateTF );
		
		dummyPanel.add ( optionsBT );
		dummyPanel.add ( showBT );
		dummyPanel.add ( overStatsBT );
		dummyPanel.add ( htmlGenBT );
		dummyPanel.setBorder(dummyBorder);


		// basic
		kdRB.setActionCommand("kill-death");
		basicDataPanel.add ( kdRB );
		hsRB.setActionCommand("head shot");
		basicDataPanel.add ( hsRB);
		dgrRB.setActionCommand("dmge given - received");
		basicDataPanel.add ( dgrRB );
		dgtRB.setActionCommand("dmge given - team");
		basicDataPanel.add ( dgtRB );
		//basicDataPanel.setPreferredSize(new Dimension(128,10));
		//basicDataPanel.setPreferredSize(128,10);

		// accuracies

		// Weap 1 Panel

		weapDataTypeCB.setBorder( BorderFactory.createEmptyBorder(3,10,10,10));
		weap0Panel.add(weapDataTypeCB);
		weap0Panel.add(new JLabel(""));
		weap0Panel.add(new JLabel(""));
		weap0Panel.add(new JLabel(""));

		knifeRB.setActionCommand("Knife");
		knifeRB.setSelectedIcon(knifeIcon2);
		weap1Panel.add(knifeRB);

		lugRB.setActionCommand("Luger");
		lugRB.setSelectedIcon(lugerIcon2);
		weap1Panel.add(lugRB);

		coltRB.setActionCommand("Colt");
		coltRB.setSelectedIcon(coltIcon2);
		weap1Panel.add(coltRB);
		
		lmineRB.setActionCommand("Land mine");		
		lmineRB.setSelectedIcon(landMineIcon2);
		weap1Panel.add(lmineRB);

		dynaRB.setActionCommand("Dynamite");
		dynaRB.setSelectedIcon(dynamiteIcon2);
		weap1Panel.add(dynaRB);

		airstrikeRB.setActionCommand("Airstrike");		
		airstrikeRB.setSelectedIcon(smokeGrenIcon2);
		weap1Panel.add(airstrikeRB);
		
		artilleryRB.setActionCommand("Artillery");
		artilleryRB.setSelectedIcon(binocularsIcon2);
		weap1Panel.add(artilleryRB);

		satchelRB.setActionCommand("Satchel");		
		satchelRB.setSelectedIcon(satchelIcon2);
		weap1Panel.add(satchelRB);

		grenadeRB.setActionCommand("Grenade");		
		grenadeRB.setSelectedIcon(grenadeIcon2);
		weap1Panel.add(grenadeRB);

		weap1Panel.add(new JLabel(""));
		weap1Panel.add(new JLabel(""));
		weap1Panel.add(new JLabel(""));


		// Weap 2 Panel


		mp40RB.setActionCommand("MP-40");
		mp40RB.setSelectedIcon(mp40Icon2);
		weap2Panel.add(mp40RB);
		
		thomRB.setActionCommand("Thompson");
		thomRB.setSelectedIcon(thompsonIcon2);
		weap2Panel.add(thomRB);
		
		stenRB.setActionCommand("Sten");
		stenRB.setSelectedIcon(stenIcon2);
		weap2Panel.add(stenRB);
		
		fg42RB.setActionCommand("FG-42");
		fg42RB.setSelectedIcon(fg42Icon2);
		weap2Panel.add(fg42RB);
		
		k43RB.setActionCommand("K43 Rifle");
		k43RB.setSelectedIcon(k43Icon2);
		weap2Panel.add(k43RB);
		
		garandRB.setActionCommand("Garand");
		garandRB.setSelectedIcon(garandIcon2);
		weap2Panel.add(garandRB);

		// Sans headshot
		panzerRB.setActionCommand("Panzer");
		panzerRB.setSelectedIcon(panzerfaustIcon2);
		weap2Panel.add(panzerRB);

		mg42RB.setActionCommand("MG-42 Gun");
		mg42RB.setSelectedIcon(mg42Icon2);
		weap2Panel.add(mg42RB);

		fthrowerRB.setActionCommand("Flame Thrower");
		fthrowerRB.setSelectedIcon(flamerIcon2);
		weap2Panel.add(fthrowerRB);

		mortarRB.setActionCommand("Mortar");
		mortarRB.setSelectedIcon(mortarIcon2);
		weap2Panel.add(mortarRB);

		grenlaunchRB.setActionCommand("G.Launcher");		
		grenlaunchRB.setSelectedIcon(gLauncherIcon2);
		weap2Panel.add(grenlaunchRB);

		weap2Panel.add(new JLabel());
		//weap3Panel.add(new JLabel());


		// skills 
		skillDataTypeCB.setBorder( BorderFactory.createEmptyBorder(3,10,10,10));
		skills0Panel.add(skillDataTypeCB);
		skills0Panel.add(new JLabel(""));

		bsenseRB.setActionCommand("Battle Sense");
		bsenseRB.setSelectedIcon(bSenseIcon2);	
		skillsButtonPanel.add(bsenseRB);

		lweaponRB.setActionCommand("Light Weapons");
		lweaponRB.setSelectedIcon(lWeaponIcon2);
		skillsButtonPanel.add(lweaponRB);

		hweaponRB.setActionCommand("Heavy Weapons");
		hweaponRB.setSelectedIcon(hWeaponIcon2);
		skillsButtonPanel.add(hweaponRB);

		signalsRB.setActionCommand("Signals");
		signalsRB.setSelectedIcon(signalsIcon2);
		skillsButtonPanel.add(signalsRB);

		covopsRB.setActionCommand("Covert Ops");
		covopsRB.setSelectedIcon(covOpsIcon2);
		skillsButtonPanel.add(covopsRB);

		engieRB.setActionCommand("Engineering");
		engieRB.setSelectedIcon(engineerIcon2);
		skillsButtonPanel.add(engieRB);

		faidRB.setActionCommand("First Aid");
		faidRB.setSelectedIcon(medicIcon2);
		skillsButtonPanel.add(faidRB);

		skillsButtonPanel.add(new JLabel(""));
		
		skillsPanel.add( skills0Panel );
		skillsPanel.add( skillsButtonPanel );

		group.add(kdRB);
		group.add(hsRB);
		group.add(dgrRB);
		group.add(dgtRB);
		group.add(lugRB);
		group.add(coltRB);
		group.add(mp40RB);
		group.add(thomRB);
		group.add(stenRB);
		group.add(fg42RB);
		group.add(garandRB);
		group.add(k43RB);
		group.add(panzerRB);
		group.add(mg42RB);
		group.add(fthrowerRB);
		group.add(mortarRB);
		
		group.add(knifeRB);
		group.add(satchelRB);
		group.add(grenadeRB);
		group.add(grenlaunchRB);
		group.add(lmineRB);
		group.add(dynaRB);
		group.add(airstrikeRB);
		group.add(artilleryRB);

		group.add(bsenseRB);
		group.add(lweaponRB);
		group.add(hweaponRB);
		group.add(signalsRB);
		group.add(covopsRB);
		group.add(engieRB);
		group.add(faidRB);


		// Set up tool tip text
		Enumeration enumeration = group.getElements();
		while (enumeration.hasMoreElements()) {
			JRadioButton currButton = (JRadioButton)enumeration.nextElement();
			currButton.setToolTipText( currButton.getActionCommand());
			//currButton.setMargin(new Insets(0,0,0,0));
			//System.out.println(currButton.getMargin().toString());
		}


		this.setLayout ( layout );	
		
		
		panelUp.add( datePanel );
		JButton logoBt = new JButton(logoIcon);
		//logoBt.setBorder(BorderFactory.createCompoundBorder(bevelLowered,BorderFactory.createEmptyBorder(8,2,2,2)));
		logoBt.setBorder(BorderFactory.createEmptyBorder(8,2,2,2));
		//logoBt.setBorder(BorderFactory.createCompoundBorder(bevelLowered,BorderFactory.createEmptyBorder(0,0,0,0)));
		logoBt.setEnabled(true);
		panelUp.add( logoBt);
		panelUp.add( dummyPanel );

		leftPanel.add ( basicDataPanel ); // Basic Data
		leftPanel.add ( skillsPanel ); // Skill

		//weaponPanel.setLayout( new BoxLayout( weaponPanel, BoxLayout.X_AXIS)); 
		//weapDataTypeCB.setAlignmentY(0.0f);
		//weapDataTypeCB.setPreferredSize( weapDataTypeCB.getMinimumSize());
		
		weaponPanel.add ( weap0Panel );   
		//weap1Panel.setAlignmentY(0.0f);
		weaponPanel.add ( weap1Panel );
		//weap2Panel.setAlignmentY(0.0f);
		weaponPanel.add ( weap2Panel );
		//weaponPanel.add ( weap3Panel );

		panelDn.add( leftPanel );
		panelDn.add( weaponPanel );

		this.add ( panelUp );
		this.add ( panelDn );


//		this.add ( weap1Panel ); // Weapons
//		this.add ( weap2Panel ); // Weapons 2
//		this.add ( panel6 ); // Weapons 3

	
	}


	public void actionPerformed(ActionEvent e) {

		if ("options".equals(e.getActionCommand())) {
			o.debug("Display Options Frame");
			OptionsFrame oframe= new OptionsFrame( "Display Options  "+ETStat.getAppNameVersion(), this.am );
			this.am.optionFrameOpening();	
			oframe.pack();
			oframe.setVisible(true);
		}

		if ("stats".equals(e.getActionCommand())) {
			JFrame tempFrame = new JFrame("Global Statistics  "+ETStat.getAppNameVersion());
			GlobalStatPanel statp = new GlobalStatPanel();

			Round.refreshAverage();
			statp.setDates(Round.get_minDate(),Round.get_maxDate());
			statp.setAverage(	Round.get_avgkill(),
								Round.get_avgdeath(),
								Round.get_avghshot(),
								Round.get_avgDG(),
								Round.get_avgDR(),
								Round.get_avgDT());
			statp.setRoundPlayed(Round.get_allRounds().size());
			tempFrame.setContentPane( statp );
			tempFrame.pack();
			tempFrame.setVisible( true );
		}
		if ("html".equals(e.getActionCommand())) {
			//System.out.println("html setup requested");

			ETQuery q = new ETQuery(startDateTF.getText(), endDateTF.getText());
			q.sort();
			while (q.processNext( ROUND_HARD_CODED_LIMIT ));
			o.debug("remaining rounds: "+Integer.toString( q.get_filteredRoundsSize() ));	

			JFrame tempFrame = new JFrame("HTML Generator "+ETStat.getAppNameVersion());
			HtmlGeneratorPanel htmlp = new HtmlGeneratorPanel(q);

			if (q.get_filteredRoundsFirst()!=null && q.get_filteredRoundsLast()!=null )
				htmlp.setDates(q.get_filteredRoundsFirst().get_endDateStr(),q.get_filteredRoundsLast().get_endDateStr());
			
			htmlp.setRoundPlayed(q.get_filteredRoundsSize());
			tempFrame.setContentPane( htmlp );
			tempFrame.pack();
			tempFrame.setVisible( true );
		}

		if ("showgraph".equals(e.getActionCommand())) {

			ETQuery q = new ETQuery(startDateTF.getText(), endDateTF.getText());
			q.sort();
			while (q.processNext( ROUND_HARD_CODED_LIMIT ));
			o.debug("remaining rounds: "+Integer.toString( q.get_filteredRoundsSize() ));	

			
			// determines radio button selected
			String action = group.getSelection().getActionCommand();

			// determines combo box selection
			int wSelector = 0;
			String weapMode = (String)weapDataTypeCB.getSelectedItem();
			if (weapMode.equals("Accuracy")) wSelector = Weapon.SELECT_ACCURACY;
			if (weapMode.equals("Kills"))	wSelector =	Weapon.SELECT_KILL;
			if (weapMode.equals("Deaths"))	wSelector =	Weapon.SELECT_DEATH;

			// determines combo box selection
			int sSelector = 0;
			String skillMode = (String)skillDataTypeCB.getSelectedItem();
			if (skillMode.equals("Level"))	sSelector = Skill.SELECT_LEVEL;
			if (skillMode.equals("Points"))	sSelector =	Skill.SELECT_POINTS;
			if (skillMode.equals("Medals"))	sSelector =	Skill.SELECT_MEDALS;

			q.set_weapDataType( wSelector );
			q.set_skillDataType( sSelector );


			if ( action.equals("kill-death") )  {
				q.prepareVectors( ETQuery.KILLDEATHDELTA);
				weapMode = new String("");
				skillMode = new String("");
			} else if ( action.equals("head shot")  ) {
				q.prepareVectors( ETQuery.HEADSHOT);
				weapMode = new String("");
				skillMode = new String("");
			} else if ( action.equals("dmge given - received") ) {
				q.prepareVectors( ETQuery.DAMAGEGR );
				weapMode = new String("");
				skillMode = new String("");
			} else if ( action.equals("dmge given - team") ) {
				q.prepareVectors( ETQuery.DAMAGEGT );
				weapMode = new String("");
				skillMode = new String("");
			} else if ( action.equals("Battle Sense") ) {
				q.prepareVectors( ETQuery.BSENSE );
				weapMode = new String("");
			} else if ( action.equals("Light Weapons") ) {
				q.prepareVectors( ETQuery.LWEAPONS );
				weapMode = new String("");
			} else if ( action.equals("Heavy Weapons") ) {
				q.prepareVectors( ETQuery.HWEAPONS );
				weapMode = new String("");
			} else if ( action.equals("Covert Ops") ) {
				q.prepareVectors( ETQuery.COVOPS );
				weapMode = new String("");
			} else if ( action.equals("Signals") ) {
				q.prepareVectors( ETQuery.SIGNALS );
				weapMode = new String("");
			} else if ( action.equals("Engineering") ) {
				q.prepareVectors( ETQuery.ENGINEERING );
				weapMode = new String("");
			} else if ( action.equals("First Aid") ) {
				q.prepareVectors( ETQuery.FIRSTAID );
				weapMode = new String("");
			} else if ( action.equals("Colt") ) {
				q.prepareVectors( ETQuery.COLT );
				skillMode = new String("");
			} else if ( action.equals("Luger") ) {
				q.prepareVectors( ETQuery.LUGER );
				skillMode = new String("");
			} else if ( action.equals("MP-40") ) {
				q.prepareVectors( ETQuery.MP40 );
				skillMode = new String("");
			} else if ( action.equals("Thompson") ) {
				q.prepareVectors( ETQuery.THOMPSON );
				skillMode = new String("");
			} else if ( action.equals("Sten") ) {
				q.prepareVectors( ETQuery.STEN );
				skillMode = new String("");
			} else if ( action.equals("FG-42") ) {
				q.prepareVectors( ETQuery.FG42 );
				skillMode = new String("");
			} else if ( action.equals("Garand") ) {
				q.prepareVectors( ETQuery.GARAND );
				skillMode = new String("");
			} else if ( action.equals("K43 Rifle") ) {
				q.prepareVectors( ETQuery.K43 );
				skillMode = new String("");
			} else if ( action.equals("Panzer") ) {
				q.prepareVectors( ETQuery.PANZER );
				skillMode = new String("");
			} else if ( action.equals("MG-42 Gun") ) {
				q.prepareVectors( ETQuery.MG42 );
				skillMode = new String("");
			} else if ( action.equals("Flame Thrower") ) {
				q.prepareVectors( ETQuery.FTHROWER );
				skillMode = new String("");
			} else if ( action.equals("Mortar") ) {
				q.prepareVectors( ETQuery.MORTAR );
				skillMode = new String("");
			} else if ( action.equals("Knife") ) {
				q.prepareVectors( ETQuery.KNIFE );
				skillMode = new String("");
			} else if ( action.equals("Satchel") ) {
				q.prepareVectors( ETQuery.SATCHEL );
				skillMode = new String("");
			} else if ( action.equals("Grenade") ) {
				q.prepareVectors( ETQuery.GRENADE );
				skillMode = new String("");
			} else if ( action.equals("G.Launcher") ) {
				q.prepareVectors( ETQuery.GLAUNCHER );
				skillMode = new String("");
			} else if ( action.equals("Land mine") ) {
				q.prepareVectors( ETQuery.LANDMINE );
				skillMode = new String("");
			} else if ( action.equals("Dynamite") ) {
				q.prepareVectors( ETQuery.DYNAMITE );
				skillMode = new String("");
			} else if ( action.equals("Airstrike") ) {
				q.prepareVectors( ETQuery.AIRSTRIKE );
				skillMode = new String("");
			} else if ( action.equals("Artillery") ) {
				q.prepareVectors( ETQuery.ARTILLERY );
				skillMode = new String("");
			} else {
				System.out.println("action : "+action+" not recognized");
				return;
			}

			String mode = new String();
			if ( skillMode.length() > 0) mode = skillMode;
			if ( weapMode.length() > 0)  mode = weapMode;

			DrawFrame dframe = null;
				
			if (mode.length() > 0)
				dframe= new DrawFrame(action+" ("+mode+") "+startDateTF.getText()+"-"+endDateTF.getText()+"  "+ETStat.getAppNameVersion(),  q, this.getSize() );
			else
				dframe= new DrawFrame(action+"  "+startDateTF.getText()+"-"+endDateTF.getText()+"  "+ETStat.getAppNameVersion(),  q, this.getSize() );


//			dframe.addWindowListener(new WindowAdapter()
//			{
//				public void windowClosing(WindowEvent e)
//				{
//					System.exit(0);
//				}
//			});
			dframe.pack();
			dframe.setVisible(true);

		}
	}
}
