package rtcw;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

class DrawPanel extends JPanel implements MouseMotionListener,MouseListener
{
	Configuration conf = Configuration.getInstance();
	MsgOutput o= new MsgOutput( MsgOutput.WARNING, "RTCW.DrawPanel" );

	static final double rad90 = java.lang.Math.toRadians( 90.0  );
	static boolean paintComponent_inProgres= false;
	FontMetrics fm  = null;
	Vector shapeVector = null; 
	Vector<String> legendVector = null;
	Vector<Long> keyVector = null;
	Vector<String> teamVector = null;
	ETQuery query= null;
	double hMargins = 10.0;
	double vMargins = 25.0;
	
	// options
	boolean textAliasing =		true;
	boolean drawMargins =		false;
	boolean useGradient =		true;
	boolean bold		=		true;
	Color singleColor1 =		Color.yellow; 
	Color singleColor2 =		Color.orange;
	Color gradientHotColor1 =	Color.red;
	Color gradientColdColor1 =	Color.orange;
	Color gradientHotColor2 =	Color.red;
	Color gradientColdColor2 =	Color.orange;
	Color backGroundColor =		Color.lightGray;
	Color fontColor =			Color.black;

	Font  font					= new Font("Dialog", Font.BOLD, 9); 

	Color [] colors = new Color[2];
	GradientPaint [] gp = new GradientPaint[2];
	boolean initialisationRequired	=	true;

	// Pour mouseListener
	XRectangle currRect = null;
	int lastSelected = -1;
	static JFrame roundInfoFrame = null;

	public DrawPanel( ETQuery q, Dimension d ) {
		
		this.query = q;
		addMouseMotionListener(this);
		addMouseListener(this);
	}
	
	// Space preserved at the top and the bottom
	public void set_vMargins ( double vm ) { this.vMargins = vm; } 

	// Space preserved left and right
	public void set_hMargins ( double hm ) { this.hMargins = hm; } 
	
	/**
	 * @param title
	 */
	public String generateHtml(String title) {
		
		StringBuffer response = new StringBuffer();
		String key= Long.toString(new Date().getTime());
	
	
//		File output = new File(key+"-index.html");
//		FileWriter outputfw = null;
//		BufferedWriter outputbw = null;
		
		// make sure this.conf is initialized
		if (initialisationRequired) {
			if (conf.isEmpty() )
				initConf();

			initAttributesBasedOnConf();
			initialisationRequired = false;
		}
  		
/*		try {
			outputfw  = new FileWriter( output );
			outputbw = new BufferedWriter(outputfw);
		} catch (IOException e1) {
			e1.printStackTrace();
			return response.toString();
		}
*/		
/*		StringBuffer sb = new StringBuffer();
		sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
		sb.append("<HTML>\n");
		sb.append("<HEAD>\n");
		sb.append("<META http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\">\n");
		sb.append("<TITLE>"+title+"</TITLE>\n");
		sb.append("</HEAD>\n").append("<BODY>\n");
*/		
		String backGroundColorStr = "#"+Integer.toHexString(backGroundColor.getRGB() & 0xffffff);
		response.append("<TABLE BGCOLOR=\""+backGroundColorStr+"\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"1\">\n");
		
		
		/*
		 * Png images Size based on DrawPanel size
		 */
		Vector<Double> valueVector =  this.query.get_valueVector();
		legendVector =			this.query.get_legendVector();
		keyVector  = 				this.query.get_keyVector();
		teamVector = 				this.query.get_teamVector();
		
		double preferredHeight= 100.0;
		double scaleY;
		StringBuffer upperRow = new StringBuffer().append("<TR>");
		StringBuffer lowerRow = new StringBuffer().append("<TR>");
		

		// Determine maximum value

		double maxValue = 0.0;
		Enumeration<Double> values = valueVector.elements();
		while (values.hasMoreElements()) {
				double currValue = ((Double)values.nextElement()).doubleValue();
				if (currValue<0) currValue= -currValue;
				if (currValue>maxValue) maxValue=currValue;
		}
		scaleY= preferredHeight / maxValue;
		
		// Reset Enum 
		
		values = valueVector.elements();
		int clrindex = 0;
		int columnCounter=0;
		int minVal=0,maxVal=0;

		while (values.hasMoreElements()) {

				BufferedImage bufferedImage = null;
				Graphics2D htmlG2d = null;

				//System.out.println("Height="+((XRectangle)sh).getHeight());
				
				int correctedWidth = 10;
				int roundedHeight = Math.round( ((Double)values.nextElement()).floatValue() );
				
				// Null value or too small value  must be skipped - no image generated
				if (roundedHeight != 0 && (int)(roundedHeight*scaleY)!=0)  {

					if (roundedHeight>maxVal) maxVal= roundedHeight;
					if (roundedHeight<minVal) minVal= roundedHeight;
					
					//System.out.println("roundedHeight="+roundedHeight);
					String pictureName = key+"-"+keyVector.elementAt(clrindex).toString()+".png";
					
					// Set up ALT or TiTLE
					Round currRound = Round.get_roundByKey(((Long)keyVector.elementAt(clrindex)).longValue());
					String dateString = new String("");
					if (currRound != null ) dateString = currRound.get_endDateStr();
					String alt = roundedHeight+" ("+ legendVector.elementAt(clrindex).toString()+" "+dateString+")" ;
					columnCounter++;
					
					
					
					if (roundedHeight>0) {
						bufferedImage = new BufferedImage(correctedWidth,(int)(roundedHeight*scaleY),BufferedImage.TYPE_INT_RGB);
						//Link not ready - upperRow.append("<TD style=\"vertical-align: bottom;\"><A HREF=\"detail.html\"><IMG BORDER=\"0\" TITLE=\""+alt+"\" ALT=\""+alt+"\" SRC=\""+pictureName+"\"></A></TD>\n");	
						upperRow.append("<TD style=\"vertical-align: bottom;\"><IMG BORDER=\"0\" TITLE=\""+alt+"\" ALT=\""+alt+"\" SRC=\""+pictureName+"\"></TD>\n");	
						lowerRow.append("<TD>&nbsp;</TD>\n");	
	 				} else { 
						bufferedImage = new BufferedImage(correctedWidth,-(int)(roundedHeight*scaleY),BufferedImage.TYPE_INT_RGB);
						//Link not ready - lowerRow.append("<TD style=\"vertical-align: top;\"><A HREF=\"detail.html\"><IMG BORDER=\"0\" TITLE=\""+alt+"\" ALT=\""+alt+"\" SRC=\""+pictureName+"\"></A></TD>\n");	
						lowerRow.append("<TD style=\"vertical-align: top;\"><IMG BORDER=\"0\" TITLE=\""+alt+"\" ALT=\""+alt+"\" SRC=\""+pictureName+"\"></TD>\n");	
						upperRow.append("<TD>&nbsp;</TD>\n");	
	 				}
	 				
	 				// Giving color attributes to Graphics2D
					int team;
					String teamName = (String)teamVector.elementAt( clrindex );
					if (teamName.equals("Axis")) team = 0;
					else team = 1;

					colors[0]= singleColor1;
					colors[1]= singleColor2;
					if (roundedHeight>0) {
						gp[0] = new GradientPaint(0, (float)bufferedImage.getHeight(), gradientColdColor1, 0, bufferedImage.getHeight()-(float)(maxValue*scaleY), gradientHotColor1, true); 
						gp[1] = new GradientPaint(0, (float)bufferedImage.getHeight(), gradientColdColor2, 0, bufferedImage.getHeight()-(float)(maxValue*scaleY), gradientHotColor2, true); 
					} else {
						gp[0] = new GradientPaint(0, (float)0, gradientColdColor1, 0, (float)(maxValue*scaleY), gradientHotColor1, true); 
						gp[1] = new GradientPaint(0, (float)0, gradientColdColor2, 0, (float)(maxValue*scaleY), gradientHotColor2,  true); 
					}
					//System.out.println("gradient point 0: 0,"+ (float)cg.get_xAxisYCoord());
		//			System.out.println("gradient point 0: 0,0");
			//		System.out.println("gradient point 1: 0,"+  (float)maxValue*zoomY);
				
				
	 				htmlG2d = bufferedImage.createGraphics();
					if (useGradient) htmlG2d.setPaint(gp[team]);
					else htmlG2d.setColor(colors[team]);
	
	 				htmlG2d.fill( new Rectangle(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight()));
					htmlG2d.dispose();
					File file = new File(pictureName);
					try {
						ImageIO.write(bufferedImage, "png", file);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			//} // End of  instanceof XRectangle

			clrindex++;
		}
		upperRow.append("</TR>");
		lowerRow.append("</TR>");
	
		response.append(upperRow.toString());
		response.append(lowerRow.toString());

		// Min Max String
		String maxMin = "(max:"+maxVal+" min:"+minVal+")";
		
		if (columnCounter < 60)
			response.append("<TR><TD COLSPAN=\""+columnCounter+"\" style=\"font-family: verdana; font-size: x-small; text-align: center;\" >"+title+" "+maxMin+"</TD></TR>");
		else 
			response.append("<TR><TD COLSPAN=\""+columnCounter+"\" style=\"font-family: verdana; font-size: x-small; text-align: left;\" >"+title+" "+maxMin+"</TD></TR>");

		response.append("</TABLE>\n");
		return response.toString();
/*		try {
			outputbw.write(sb.toString());
			outputbw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
*/	
	}
	
	public void paintComponent(Graphics g) {
		clear(g);

		o.debug( "entering JPanel.painComponent.");
		o.debug( "entering JPanel.painComponent. max height : "+this.getMaximumSize().getHeight());
		o.debug( "entering JPanel.painComponent. max with : "+this.getMaximumSize().getWidth());

		if (initialisationRequired) {
			if (conf.isEmpty() )
				initConf();

			initAttributesBasedOnConf();
			initialisationRequired = false;
		}

		o.debug( "JPanel.painComponent - Configuration stuff ended");

		this.setBackground( backGroundColor );

		ChartGenerator cg = new ChartGenerator ();
		Dimension innerDrawingDim = 
			new Dimension((int)(this.getSize().getWidth() - 2 * this.hMargins),
			(int)(this.getSize().getHeight() - 2 * this.vMargins));
		
		shapeVector  = cg.acquireData ( innerDrawingDim, this.query.get_valueVector(), 10.0, 12.0 ); 
		legendVector =	this.query.get_legendVector();
		keyVector  = this.query.get_keyVector();
		teamVector = this.query.get_teamVector();

		o.debug( "JPanel.painComponent - All data acquired from ChartGenerator and ETQuery");
		o.debug( "JPanel.painComponent - shape to draw = "+shapeVector.size());

		double preferedWidth = cg.get_shapeMaxX() +  2 * this.hMargins;
		this.setPreferredSize( new Dimension ((int)preferedWidth, (int)this.getPreferredSize().getHeight() ));
		//this.setMinimumSize( new Dimension ((int)preferedWidth, (int)this.getPreferredSize().getHeight() ));
		//System.out.println("JPanel.paintComponent - preferredSize : "+Integer.toString((int)preferedWidth)+","+Integer.toString((int)this.getPreferredSize().getHeight() ));



		//Font f = new Font("Dialog", Font.PLAIN, 9);
		Font f = font;
		
		g.setFont(f);
		Graphics2D g2d = (Graphics2D)g;

		// Display H & V margins
		if ( drawMargins )
			g2d.draw(new Rectangle2D.Double(hMargins,vMargins, this.getSize().getWidth()-2*hMargins, this.getSize().getHeight()-2*vMargins));

		// shapes must be translate according to margins
		g2d.translate ( hMargins, vMargins );

		//g2d.draw(new java.awt.geom.Line2D.Double(0, cg.get_xAxisYCoord(), this.getSize().getWidth()-2*hMargins, cg.get_xAxisYCoord()) );
		//g2d.draw(new java.awt.geom.Line2D.Double(0, cg.get_maxAsbValueCoordY(), this.getSize().getWidth()-2*hMargins, cg.get_maxAsbValueCoordY()) );
		colors[0]= singleColor1;
		colors[1]= singleColor2;
		gp[0] = new GradientPaint(0, (float)cg.get_xAxisYCoord(), gradientColdColor1, 0, (float)cg.get_maxAsbValueCoordY(), gradientHotColor1, true); 
		gp[1] = new GradientPaint(0, (float)cg.get_xAxisYCoord(), gradientColdColor2, 0, (float)cg.get_maxAsbValueCoordY(), gradientHotColor2,  true); 

		o.debug( "JPanel.painComponent - Entering Drawing Shape Phase");
		// 1- Draw the shapes
		Enumeration shapes = this.shapeVector.elements();
		int clrindex = 0;
		int team = 0; // Team = Axis (0)  or Allies (1) 
		
		while (shapes.hasMoreElements()) {

			Shape sh = ( Shape )shapes.nextElement();
			o.debug( "JPanel.painComponent - painting shape");

			// Coloring Axis/Allies has only sense with XRectangle - at the end we draw the X Axis
			if ( sh instanceof XRectangle )	{
				String teamName = (String)teamVector.elementAt( clrindex );
				if (teamName.equals("Axis")) team = 0;
				else team = 1;

			}

			if (useGradient) g2d.setPaint(gp[team]);
			else g2d.setColor( colors[team]);
			
			clrindex++;
			g2d.fill( sh );

			g2d.setColor( Color.black );
			g2d.draw( sh );
		}
		
		// Paint selection last
		if (currRect != null) {
			g2d.setColor( Color.white );
			g2d.draw( currRect );
		}



		// Text Aliasing
		if (this.textAliasing)	{
			g2d.addRenderingHints( 
				new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON) );
		} else {
			g2d.addRenderingHints( 
				new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF) );
		}

		o.debug( "JPanel.painComponent - Entering Drawing Text Phase");
		// 2- Add inner text - text up / to down
		String legend = null;

		g2d.translate ( this.getSize().getWidth() - 2 * this.hMargins, 0);
		g2d.rotate ( rad90 );

		Enumeration shapes2 = this.shapeVector.elements();
		int ctrlgd = 0;

		while (shapes2.hasMoreElements()) {
			Shape  sh = ( Shape )shapes2.nextElement();

			if (sh instanceof XRectangle )	{
				legend = ( String )legendVector.elementAt(ctrlgd);
				ctrlgd++;

				XRectangle xr = (XRectangle)sh;

				if ( xr.get_value() < 0.0 )	{

					double x =		xr.getY();
					double y =		this.getSize().getWidth() - 2*this.hMargins - xr.getX() - xr.getWidth() ;
					double width =	xr.getHeight();
					double height =	xr.getWidth();

					fm = g2d.getFontMetrics();
					Rectangle2D fontrect= fm.getStringBounds( xr.get_valueStr()+"0", g2d );
					//g2d.setClip( new Rectangle((int)Math.round(x + xr.getHeight() + 5.0), (int)Math.round( y + xr.getWidth() -3.0), (int)Math.round(fontrect.getWidth()), (int)Math.round(fontrect.getHeight())) );
					
					g2d.setClip( new Rectangle((int)(x + width+2) , (int)y-1 , (int)(fontrect.getWidth()), (int)height+1) );
					//g2d.setColor(Color.red);
					//g2d.fill( new Rectangle((int)(x + width+2) , (int)y-1 , (int)(fontrect.getWidth()), (int)height+1)  );
					g2d.setColor(fontColor);
					g2d.drawString (xr.get_valueStr(),(float)(x + xr.getHeight() + 5.0), (float)( y + xr.getWidth() -3.0));

					g2d.setClip( new Rectangle((int)x, (int)y , (int)width -2, (int)height) );
					g2d.setColor(fontColor);
					g2d.drawString (legend,(float)(x + 2.0), (float)( y + xr.getWidth() -3.0));
				}
			}
		}

		// 3- Add inner text - text down / to up
		g2d.translate ( this.getSize().getHeight() - 2 * this.vMargins, this.getSize().getWidth() - 2 * this.hMargins );
		g2d.rotate ( - rad90 * 2.0 );

		Enumeration shapes3 = this.shapeVector.elements();
		ctrlgd = 0;

		while (shapes3.hasMoreElements()) {
			Shape sh = ( Shape )shapes3.nextElement();

			if (sh instanceof XRectangle )	{
				legend = ( String )legendVector.elementAt(ctrlgd);
				ctrlgd++;

				XRectangle xr = (XRectangle)sh;
				
				if ( xr.get_value() > 0.0 )	{
					double x =		this.getSize().getHeight() - 2*this.vMargins - xr.getY() - xr.getHeight();
					double y =		xr.getX();
					double width =	xr.getHeight();
					double height =	xr.getWidth();

					fm = g2d.getFontMetrics();
					Rectangle2D fontrect= fm.getStringBounds( xr.get_valueStr()+"0", g2d );
					g2d.setClip( new Rectangle((int)(x+width), (int)y , (int)(fontrect.getWidth()), (int)height) );
					//g2d.setColor(Color.red);
					//g2d.fill ( new Rectangle((int)(x+width), (int)y , (int)(fontrect.getWidth()), (int)height) );
					g2d.setColor(fontColor);
					g2d.drawString (xr.get_valueStr(),(float)(x + xr.getHeight() + 2.0), (float)( y + xr.getWidth() -2.0));
					
					//g2d.draw( new Rectangle((int)x, (int)y , (int)width, (int)height));	
					g2d.setClip( new Rectangle((int)x, (int)y , (int)width - 2, (int)height) );
					g2d.setColor(fontColor);
					g2d.drawString (legend,(float)(x + 2.0), (float)( y + xr.getWidth() -2.0));

				}
			}
		}

		
	}

	protected void clear(Graphics g) {
		super.paintComponent(g);
	}

	public void set_textAliasing ( boolean a ) {
		this.textAliasing = a;
	}

	public void mouseDragged(MouseEvent e) {}
	
	// We must take care of hMargins & vMargins
	// 1- for determining which XRectangle object
	// 2- for determining repaint area

	public void mouseMoved(MouseEvent e) {

		boolean localized = false;
		boolean repaintFast = true; // means that areas to repaint are communicated - not whole panel
		
		// no shapeVector means nothing has been drawn yet 
		if ( shapeVector == null ) return;

		// we won t do the cast 1000 times
		int hMarginsAsInt = (int)hMargins;
		int vMarginsAsInt = (int)vMargins;

		//When drawing shape we did g2d.translate ( hMargins, vMargins );
		//So we do a translate ( hMargins, vMargins ) on coords mouse too
		int x = e.getX() - hMarginsAsInt;
		int y = e.getY() - vMarginsAsInt;

		// Same Rectangle is selected - return - nothing to do
		if ( currRect != null && currRect.contains( x,y ) )	return;

		// else currRect has changed - so repaint of old currRect is required
		// mark old currRect as dirty
		if ( currRect != null && repaintFast)
			this.repaint( new Rectangle((int)currRect.getX()-1+hMarginsAsInt,(int)currRect.getY()-1+vMarginsAsInt,(int)currRect.getWidth()+2,(int)currRect.getHeight()+3)); 

		int counter = 0;
		while ( counter < shapeVector.size() && !localized)	{

			if ( shapeVector.elementAt(counter) instanceof XRectangle )	{
				XRectangle xr = (XRectangle)shapeVector.elementAt(counter);

				if (  xr.contains( x,y ) ) {
					currRect = xr;
					// mark new currRect as dirty
					if (repaintFast)
						this.repaint( new Rectangle((int)currRect.getX()-1+hMarginsAsInt,(int)currRect.getY()-1+vMarginsAsInt,(int)currRect.getWidth()+2,(int)currRect.getHeight()+3)); 
					//colors[counter]= Color.red ;
					localized = true;
					lastSelected = counter;
				}
			}
			counter++;
		}

	}


// Interface MouseListener

	public void mouseClicked(MouseEvent e) {
		// Get the index of last selected Rectangle/Round
		if (lastSelected != -1 && roundInfoFrame == null)  {
			Round fr = Round.get_roundByKey(((Long)keyVector.elementAt(lastSelected)).longValue());	
			o.info( "Round ="+fr.toString()+" "+fr.get_teamName()+" "+fr.get_fileName() );			
			roundInfoFrame = new RoundInfoFrame( "Click on bar chart to close me  "+ETStat.getAppNameVersion(),fr );
			roundInfoFrame.addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent e) {
					DrawPanel.roundInfoFrameClosedNotify();
				}
			});

			
			roundInfoFrame.pack();
			roundInfoFrame.setVisible(true);
			return;
		}
		if ( roundInfoFrame != null ) {
			DrawPanel.roundInfoFrameClosedNotify();
		}
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

	static public void roundInfoFrameClosedNotify() {
		if ( roundInfoFrame.isVisible() )	roundInfoFrame.setVisible(false);
		roundInfoFrame.dispose();
		roundInfoFrame = null;
		return;
	}

	private void initConf() {
		conf.setValue("Color"+ColorChooserPanel.NOGRADIENTCOLOR1, "0x"+Integer.toHexString(singleColor1.getRGB() & 0xffffff) );
		conf.setValue("Color"+ColorChooserPanel.NOGRADIENTCOLOR2, "0x"+Integer.toHexString(singleColor2.getRGB() & 0xffffff) );
		conf.setValue("Color"+ColorChooserPanel.GRADIENTCOLORHOT1, "0x"+Integer.toHexString(gradientHotColor1.getRGB() & 0xffffff) );
		conf.setValue("Color"+ColorChooserPanel.GRADIENTCOLORCOLD1, "0x"+Integer.toHexString(gradientColdColor1.getRGB() & 0xffffff) );
		conf.setValue("Color"+ColorChooserPanel.GRADIENTCOLORHOT2, "0x"+Integer.toHexString(gradientHotColor2.getRGB() & 0xffffff) );
		conf.setValue("Color"+ColorChooserPanel.GRADIENTCOLORCOLD2, "0x"+Integer.toHexString(gradientColdColor2.getRGB() & 0xffffff) );
		conf.setValue("Color"+ColorChooserPanel.VALUESCOLOR, "0x"+Integer.toHexString(fontColor.getRGB() & 0xffffff) );
		conf.setValue("Color"+ColorChooserPanel.BACKGROUNDCOLOR, "0x"+Integer.toHexString(backGroundColor.getRGB() & 0xffffff) );
		conf.setValue("UseGradient",String.valueOf(useGradient));
		conf.setValue("UseBold",String.valueOf(bold));
		conf.setValue("Font",font.getFamily());
		conf.setValue("AntiAlias",String.valueOf(textAliasing));
		conf.setValue("OnlyPName",new String(""));
		conf.save();
		o.warning("Configuration file not found. Default configuration file saved.");
	}

	private void initAttributesBasedOnConf() {
		useGradient=			Boolean.valueOf(conf.getValue("UseGradient")).booleanValue();
		bold		=			Boolean.valueOf(conf.getValue("UseBold")).booleanValue();
		textAliasing =			Boolean.valueOf(conf.getValue("AntiAlias")).booleanValue();
		if (bold) {
			font = new Font(conf.getValue("Font"), Font.BOLD, 9);
		} else {
			font = new Font(conf.getValue("Font"), Font.PLAIN, 9);
		}

		if (!useGradient) {
			singleColor1 =			Color.decode(conf.getValue("Color"+ColorChooserPanel.NOGRADIENTCOLOR1)); 
			singleColor2 =			Color.decode(conf.getValue("Color"+ColorChooserPanel.NOGRADIENTCOLOR2));		
		} else {
			gradientHotColor1 =		Color.decode(conf.getValue("Color"+ColorChooserPanel.GRADIENTCOLORHOT1));
			gradientColdColor1 =	Color.decode(conf.getValue("Color"+ColorChooserPanel.GRADIENTCOLORCOLD1));
			gradientHotColor2 =		Color.decode(conf.getValue("Color"+ColorChooserPanel.GRADIENTCOLORHOT2));
			gradientColdColor2 =	Color.decode(conf.getValue("Color"+ColorChooserPanel.GRADIENTCOLORCOLD2));
		}
		fontColor =				Color.decode(conf.getValue("Color"+ColorChooserPanel.VALUESCOLOR));
		backGroundColor =		Color.decode(conf.getValue("Color"+ColorChooserPanel.BACKGROUNDCOLOR));
	}




}
