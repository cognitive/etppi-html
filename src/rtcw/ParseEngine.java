package rtcw;

import java.io.BufferedReader;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import java.util.Calendar;

class ParseEngine 
{
	private FileUtil fu = FileUtil.getInstance();
	private MsgOutput o = new MsgOutput( MsgOutput.WARNING,"RTCW.ParseEngine" );
	private BufferedReader reader = null;

	private Pattern reccordEndCPattern = Pattern.compile("^Stats recorded: ([0-9][0-9]):([0-9][0-9]):([0-9][0-9]) \\(([0-9][0-9]) ([A-Z][a-z][a-z]) ([0-9][0-9][0-9][0-9])\\)$");
	private Pattern mapnameCPattern = Pattern.compile("^>>> Map: (.+)");
	private Pattern playrNameCPattern = Pattern.compile("^Overall stats for:\\s+(.+)\\s+\\([0-9] Rounds?\\)");

	private Pattern damageGTCPattern = Pattern.compile("^Damage Given: ([0-9]+)\\s+Team Damage: ([0-9]+)");
	private Pattern damageRCPattern = Pattern.compile("^Damage Recvd: ([0-9]+)");
	private Pattern XPRankCPattern = Pattern.compile("^Rank: ([A-Za-z]+) \\(([0-9]+) XP\\)");

	private Pattern skillCPattern = Pattern.compile(	"^(Battle Sense|Light Weapons|Heavy Weapons|Signals|Covert Ops|Engineering|First Aid)\\s+([0-9]) \\(([^/]+).*\\)\\s+([0-9])");

// Fix 29/09/2003
//	Kill:			[ ]{0,2}[0-9]{1,3}	3 pos
//	Death:		[ ]{0,2}[0-9]{1,3}	3 pos
//	Suicide:	[ ]{0,2}[0-9]{1,3}	3 pos
//	TK:			[ ]{0,1}[0-9]{1,2}	2 pos
//	Eff:			[ ]{0,2}[0-9]{1,3}	3 pos
//	GP:			[ ]{0,2}[0-9\-]{1,3}	3 pos
//	DG:			[ ]{0,4}[0-9]{1,5}	5 pos
//	DR:			[ ]{0,4}[0-9]{1,5}	5 pos
// missing space before TD 
//	TD:			[ ]{0,4}[0-9]{1,5}	5 pos
//	Score:		[ ]{0,5}[0-9-]{1,6}	6 pos
	private Pattern scoreCPattern = Pattern.compile("^(Axis|Allies) +.+ +[ ]{0,2}([0-9]{1,3}) [ ]{0,2}([0-9]{1,3}) [ ]{0,2}([0-9]{1,3}) [ ]{0,1}([0-9]{1,2}) [ ]{0,2}([0-9]{1,3}) [ ]{0,2}([0-9-]{1,3}) [ ]{0,4}([0-9]{1,5}) [ ]{0,4}([0-9]{1,5})[ ]{0,4}([0-9]{1,5}) [ ]{0,5}([0-9-]{1,6})$");

	//private Pattern scoreCPattern = Pattern.compile("^(Axis|Allies) +.+ +([0-9]+) +([0-9]+) +([0-9]+) +([0-9]+) +([0-9]+) +-?[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+$");
	private Pattern weapHSCPattern = Pattern.compile( "^(.........): (.....) (....[/ ]....) (.....)  (.....)     (.....)$");
	private Pattern weapCPattern = Pattern.compile( "^(.........): (.....) (....[/ ]....) (.....)  (.....)$");


	private Calendar calendar = new GregorianCalendar();
	private Round currRound =  null;

	public boolean ParserEntryPoint( String fileName ) {
		
		boolean skip;
		reader = fu.GetReader ( fileName );
		//System.out.println("file name :" +fileName);
		currRound = new Round( fileName );
		
		try {
			String line = reader.readLine();
			while ( line != null )	{
				
				
				skip = false;

				// Weapon stat

				// Flame Thrower bug !! - skip
				if ( !skip && line.matches ("F.Thrower: [0-9][0-9][0-9][0-9]\\.[0-9].*") ) {
					skip = true;
				}
				
				if ( !skip && line.matches ("^(Luger|Colt|MP-40|Thompson|F.Thrower|MG-42 Gun|Sten|FG-42|Panzer|Garand|K43 Rifle).*")) {
					String weap = PerlLike.BasicExtractionCP ( line, weapHSCPattern, 1 );
					String accu = PerlLike.BasicExtractionCP ( line, weapHSCPattern, 2 );
					String hits = PerlLike.BasicExtractionCP ( line, weapHSCPattern, 3 );
					String kills = PerlLike.BasicExtractionCP ( line, weapHSCPattern, 4 );
					String death = PerlLike.BasicExtractionCP ( line, weapHSCPattern, 5 );
					String hshot = PerlLike.BasicExtractionCP ( line, weapHSCPattern, 6 );

					if (!kills.equals("     ")) currRound.incrkill( Integer.parseInt(kills.trim()) );
					if (!death.equals("     ")) currRound.incrdeath( Integer.parseInt(death.trim()) );
					if (!hshot.equals("     ")) currRound.incrhshot( Integer.parseInt(hshot.trim()) );
				
					double WaccuDble = 0.0;
					int Wkill = 0, Wdeath = 0, Whshot = 0;
					if (!accu.equals ("     "))  WaccuDble = Double.parseDouble(accu.trim());
					if (!kills.equals("     "))  Wkill  = Integer.parseInt(kills.trim());
					if (!death.equals("     "))  Wdeath = Integer.parseInt(death.trim());
					if (!hshot.equals("     "))  Whshot = Integer.parseInt(hshot.trim());
					
					currRound.addWeapon ( new AccurateWeapon( weap, WaccuDble, hits, Wkill, Wdeath, Whshot) );  
					skip = true;
				}
				if ( !skip && line.matches ("^(Grenade|Mortar|Dynamite|Airstrike|Satchel|Landmine|Artillery|G.Launchr|Knife).*") ) {
					String weap = PerlLike.BasicExtractionCP ( line, weapCPattern, 1 );
					String accu = PerlLike.BasicExtractionCP ( line, weapCPattern, 2 );
					String hits = PerlLike.BasicExtractionCP ( line, weapCPattern, 3 );
					String kills = PerlLike.BasicExtractionCP ( line, weapCPattern, 4 );
					String death = PerlLike.BasicExtractionCP ( line, weapCPattern, 5 );
					//System.out.println("weap:"+weap+" accu:"+accu+" hits:"+hits+" kills:"+kills+" death:"+death); 

					if (!kills.equals("     ")) currRound.incrkill( Integer.parseInt(kills.trim()) );
					if (!kills.equals("     ")) currRound.incrdeath( Integer.parseInt(death.trim()) );

					double WaccuDble = 0.0;
					int Wkill = 0, Wdeath = 0;
					if (!accu.equals ("     "))  WaccuDble = Double.parseDouble(accu.trim());
					if (!kills.equals("     "))  Wkill  = Integer.parseInt(kills.trim());
					if (!death.equals("     "))  Wdeath = Integer.parseInt(death.trim());
					
					currRound.addWeapon ( new Weapon( weap, WaccuDble, hits, Wkill, Wdeath) );  
					skip = true;
				}
			
				// We don't care about them for now
				if ( !skip && line.matches ("^(SmokeScrn|Syringe).*") ) {
					skip = true;
				}

				if ( !skip && line.matches ("^([^A][^xl].......):.*") ) {
					String weap = PerlLike.BasicExtractionCP ( line, Pattern.compile("^(.........):.*"), 1 );
					System.out.println("error: weap "+weap+" not recognized !!!");
					skip = true;
				}
				
				// Overall stats for: Vaticool (0 Rounds) 
				if ( !skip && line.matches("^Overall stats for:\\s+(.+)\\s+\\([0-9] Rounds*\\)$")) {
					String pName = PerlLike.BasicExtractionCP ( line, playrNameCPattern, 1 );
					currRound.set_playerName( pName );
					skip = true;
				}

				// Fix 29/09/2003
				// Fix 26/02/2004
				if ( !skip && line.matches("^(Axis|Allies) +.+ +[ ]{0,2}[0-9]{1,3} [ ]{0,2}[0-9]{1,3} [ ]{0,2}[0-9]{1,3} [ ]{0,1}[0-9]{1,2} [ ]{0,2}[0-9]{1,3} [ ]{0,2}[0-9-]{1,3} [ ]{0,4}[0-9]{1,5} [ ]{0,4}[0-9]{1,5}[ ]{0,4}[0-9]{1,5} [ ]{0,5}[0-9-]{1,6}$")) {
					//System.out.println("round "+currRound.toString()+" matched:"+line);
					skip = true;
					String pname = currRound.get_playerName();
					if ( pname != null ) {
						// This is a FIX - 28/09/2003
						if (pname.length()>15) {
							pname= pname.substring(1,15);
							o.debug("Player name shortened to: >"+pname+"< (fix 28/09/2003)");
						}
 						if ( line.indexOf(pname) != -1  ) {
							String teamName = PerlLike.BasicExtractionCP ( line, scoreCPattern, 1 ); 
							int kill = Integer.parseInt(PerlLike.BasicExtractionCP ( line, scoreCPattern, 2 ));
							int death = Integer.parseInt(PerlLike.BasicExtractionCP ( line, scoreCPattern, 3 ));
							int suicide = Integer.parseInt(PerlLike.BasicExtractionCP ( line, scoreCPattern, 4 ));
							int tk = Integer.parseInt(PerlLike.BasicExtractionCP ( line, scoreCPattern, 5 ));
							int eff = Integer.parseInt(PerlLike.BasicExtractionCP ( line, scoreCPattern, 6 ));
							currRound.set_killDeathTk(kill, death, suicide, tk, eff);
							currRound.set_teamName ( teamName ); 
						}
					} else {
						System.out.println("Warning player name not found in "+fileName);
					}
				}


				// Battle Sense   1 (35/50)          0
				if ( !skip && 
					line.matches("^(Battle Sense|Light Weapons|Heavy Weapons|Signals|Covert Ops|Engineering|First Aid)\\s+[0-9] \\([^/]+.*\\)\\s+[0-9]$")) {
					String skillType = PerlLike.BasicExtractionCP ( line, skillCPattern, 1 );

					int stype = 0;
					if (stype == 0 && skillType.equals("Battle Sense"))		{ stype= Skill.BATTLE_SENSE; }
					if (stype == 0 && skillType.equals("Light Weapons"))	{ stype= Skill.LIGHT_WEAPONS; }
					if (stype == 0 && skillType.equals("Heavy Weapons"))	{ stype= Skill.HEAVY_WEAPONS; }
					if (stype == 0 && skillType.equals("Signals"))			{ stype= Skill.SIGNALS; }
					if (stype == 0 && skillType.equals("Covert Ops"))		{ stype= Skill.COVERT_OPS; }
					if (stype == 0 && skillType.equals("Engineering"))		{ stype= Skill.ENGINEERING; }
					if (stype == 0 && skillType.equals("First Aid"))	    { stype= Skill.FIRST_AID; }

					int level = Integer.parseInt(PerlLike.BasicExtractionCP ( line, skillCPattern, 2 ));
					int point = Integer.parseInt(PerlLike.BasicExtractionCP ( line, skillCPattern, 3 ));
					int medal = Integer.parseInt(PerlLike.BasicExtractionCP ( line, skillCPattern, 4 ));
					currRound.addSkill ( stype, level, point, medal );
					skip = true;
					//System.out.println("matched skil : "+skillType+" point = "+point);
				}

				// Rank: Gefreiter (122 XP)
				if ( !skip && line.matches("^Rank: [A-Za-z]+ \\([0-9]+ XP\\)$")) {
					String rank = PerlLike.BasicExtractionCP ( line, XPRankCPattern, 1 );
					int xp = Integer.parseInt(PerlLike.BasicExtractionCP ( line, XPRankCPattern, 2 ));
					currRound.set_exp( xp, rank );
					skip = true;
					//System.out.println("matched exp rk");
				}
				
				// Damage Given: 3181    Team Damage: 215
				if ( !skip && line.matches("^Damage Given: [0-9]+\\s+Team Damage: [0-9]+$")) {
					int gdmg =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, damageGTCPattern, 1 ));
					int tdmg =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, damageGTCPattern, 2 ));
					currRound.set_damageGT( gdmg, tdmg );
					skip = true;
					//o.debug("Team Damage: "+Integer.toString(tdmg));
					//System.out.println("matched dmg g t"+PerlLike.BasicExtractionCP ( line, damageGTCPattern, 1 ));
				}

				// Damage Recvd: 2170
				if ( !skip && line.matches("^Damage Recvd: [0-9]+$")) {
					int rdmg =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, damageRCPattern, 1 ));
					currRound.set_damageR( rdmg );
					skip = true;
					//System.out.println("matched dmg r : "+PerlLike.BasicExtractionCP ( line, damageRCPattern, 1 ));
				}

				// >>> Map: radar
				if ( !skip && line.matches("^>>> Map: .+$")) {
					String mapName = PerlLike.BasicExtractionCP ( line, mapnameCPattern, 1 );
					currRound.set_mapName( mapName );
					skip = true;
					//System.out.println("matched map name");
				}

				// Stats recorded: 13:07:09 (09 Jun 2003)
				if ( !skip && line.matches("^Stats recorded: [0-9][0-9]:[0-9][0-9]:[0-9][0-9] \\([0-9][0-9] [A-Z][a-z][a-z] [0-9][0-9][0-9][0-9]\\)$")) {
					int hours =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, reccordEndCPattern, 1 ));
					int mins =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, reccordEndCPattern, 2 ));
					int secs =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, reccordEndCPattern, 3 ));
					int day =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, reccordEndCPattern, 4 ));
					int year =		Integer.parseInt(PerlLike.BasicExtractionCP ( line, reccordEndCPattern, 6 ));
					String monthStr =	PerlLike.BasicExtractionCP ( line, reccordEndCPattern, 5 );

					int month = 0;
					boolean found=false;
					String [] monthArr = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
					
					while ( month<12 && !found ) {
						if ( monthStr.equals(monthArr[month]) )  found = true;
						else month++;
					}

					// Set calendar to start of reccord
					calendar.set(year,month,day,hours,mins,secs);
					currRound.set_endDate ( calendar.getTime().getTime()  / 1000  );
					skip = true;
					//System.out.println("matched end rec");
				}	
				line = reader.readLine(); 
			}
		}
		catch ( java.io.IOException exception ) {
			exception.printStackTrace();
			return false;
		}

		isTeamNameDetermined();
		return true;
	}

	// All objects previously created by parseEngine are deleted
	public int reset () {
		int numobjects =  Round.reset();
		
		o.info("ParseEngine reset: "+numobjects+" root objects deleted.");
		return numobjects;
	}
	/** isTeamNameDetermined
	 * is used to check that team of player has been determined before end of file<BR>
	 * In the past, undetermined team produced chart display <B>crash</B>
	 * @return boolean 
	 */
	private boolean isTeamNameDetermined() {
		boolean teamNameDetermined = false;
		String teamName = currRound.get_teamName();
		
		if (teamName == null) {
			o.severe("Teamname still not determined at the end of the round. playername:>"+currRound.get_playerName().toString()+"<");
			o.severe("Teamname still not determined at the end of the round. filename:>"+currRound.get_fileName()+"<");
			o.severe("Please send the file "+currRound.get_fileName()+" to the author");
			boolean removed = Round.removeRound(currRound);
			o.severe("Round from file "+currRound.get_fileName()+" will be ignored - "+Boolean.toString(removed));
		}
		return teamNameDetermined;
	}}

