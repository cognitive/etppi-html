package rtcw;

import java.util.Vector;
import java.util.Enumeration;
import java.awt.Dimension;
import java.text.DecimalFormat;

// This class must create shapes 
// (based on window size)  
// that will represent each value 
// of the values vector + x axis
// NB: This class won't draw anything
// NB: It will fully use an area of d (Dimension) size 
// Warning: One additionnal XLine is appended @ the end of vector

public class ChartGenerator  { 

	double shapeMaxX = 0.0;
	double xaxispix = 0.0;
	double maxAsbValueCoordY = 0.0;

	public Vector acquireData ( Dimension d, Vector<Double> values, double minbarwidth, double maxbarwidth ) {
		

		double height = d.getHeight();
		double width  = d.getWidth();
		double minval = 0.0;
		double maxval = 0.0;
		Vector shapeSet = new Vector();
		DecimalFormat df = new DecimalFormat();

		boolean firstIter = true;
		int numvalues = values.size();
		

		// Width dedicated to each sample
		// Can not be less than minbarwitdh or more than maxbarwidth
		double samplewidth = width / (double)numvalues;
		if (samplewidth> maxbarwidth) { samplewidth = maxbarwidth; }
		if (samplewidth< minbarwidth) { samplewidth = minbarwidth; }
		this.shapeMaxX = samplewidth * numvalues;
		
		// Manage Height
		Enumeration<Double> valuesEnum = values.elements();
		while (valuesEnum.hasMoreElements()) {
			double currval = ((Double)valuesEnum.nextElement()).doubleValue();
			if ( firstIter ) {
				minval = currval;
				maxval = currval;
				firstIter = false;
			} else {
				if ( currval < minval ) minval = currval;
				if ( currval > maxval ) maxval = currval;
			}
		}
		// PATCH 06/07/2003 
		// WARNING: We could have 3 cases
		// 1- Values are lower and greater than 0
		// 2- Values are lower than 0
		// 3- Values are greater than 0
		// We want 0 axis always be drawn -> see below & impact heightScale

		if (minval > 0.0) 	{ minval = 0.0;	}
		if (maxval < 0.0) 	{ maxval = 0.0;	}		
		double heightScale  = (height - 1) / (maxval - minval);


//		System.out.println("Height : "+ Double.toString( height -1 ));
//		System.out.println("Width : "+ Double.toString( width ));
//		System.out.println("# of Sample : "+ Double.toString( (double)numvalues ));
//		System.out.println("Max : "+ Double.toString( maxval ));
//		System.out.println("Min : "+ Double.toString( minval ));
//		System.out.println("Delta :"+Double.toString( maxval - minval ));
//		System.out.println("Witdh for each : "+ Double.toString( samplewidth ));
//		System.out.println("Height scale : "+ Double.toString( heightScale ));
//		
		
		// Building shape
		Enumeration<Double> valuesEnum2 = values.elements();

		// Pas de partie decimale
		String formattedVal;
		df.setMaximumFractionDigits( 0 );
		df.setMinimumFractionDigits( 0 );
		df.setNegativePrefix("");

		///////////////////////////////
		// Valeurs negatives uniquement
		//////////////////////////////
		if ( minval < 0.0 && maxval <= 0.0) {

			//X axis line is at the top 
			xaxispix = 0;
			double y = 0.0;
			double currheight;
			int counter = 0;
			

			while (valuesEnum2.hasMoreElements()) {
				double currval = ((Double)valuesEnum2.nextElement()).doubleValue();
			
				y = 0;
				currheight = -currval * heightScale;    
				
				// Value to display
				if ( currval != 0.0 ) formattedVal = df.format(currval);
				else formattedVal = new String(""); 
					
				shapeSet.add( new XRectangle( counter*samplewidth, y, samplewidth, currheight, formattedVal, currval, "toto"));  	
				//System.out.println("Rect for : "+Double.toString(currval)+"->"+Double.toString(y)+","+Double.toString(currheight));		
				counter++;
			}
			shapeSet.add( new XLine ( 0.0, xaxispix, width-1.0, xaxispix, "uou") );  
			maxAsbValueCoordY = -minval * heightScale; 
		}

		
		///////////////////////////////
		// Valeurs positives ou nulles 
		//////////////////////////////
		if ( minval >= 0.0 && maxval > 0.0) {

			//X axis line is at the bottom 
			xaxispix = height -1 ;
			double y = 0.0;
			double currheight;
			int counter = 0;
			

			while (valuesEnum2.hasMoreElements()) {
				double currval = ((Double)valuesEnum2.nextElement()).doubleValue();
			
				y = xaxispix -(currval * heightScale);
				currheight = currval * heightScale;    
				
				// Value to display
				if ( currval != 0.0 ) formattedVal = df.format(currval);
				else formattedVal = new String(""); 
					
				shapeSet.add( new XRectangle( counter*samplewidth, y, samplewidth, currheight, formattedVal, currval, "toto"));  	
				//System.out.println("Rect for : "+Double.toString(currval)+"->"+Double.toString(y)+","+Double.toString(currheight));		
				counter++;
			}
			shapeSet.add( new XLine ( 0.0, xaxispix, width-1.0, xaxispix, "uou") );  
			maxAsbValueCoordY = 0; 
		}

		//////////////////////////////////////////////////////
		// Valeurs negatives & positives : -2 a +3 par exemple
		//////////////////////////////////////////////////////
		if ( minval < 0.0 && maxval > 0.0) {

			//X axis line is placed according to where 0 value is 
			xaxispix = 	(height -1) + (minval * heightScale);
			double y = 0.0;
			double currheight;
			int counter = 0;

			while (valuesEnum2.hasMoreElements()) {
				double currval = ((Double)valuesEnum2.nextElement()).doubleValue();

				if ( currval < 0.0 ) {
					y = xaxispix;
					currheight = -(currval * heightScale);    
				} else {
					y = xaxispix -(currval * heightScale);
					currheight = currval * heightScale;    
				}
				// Value to display
				if ( currval != 0.0) formattedVal = df.format(currval);
				else formattedVal = new String(""); 

				shapeSet.add( new XRectangle( counter*samplewidth, y, samplewidth, currheight, formattedVal, currval,  "toto"));  	
				//System.out.println("Rect for : "+Double.toString(currval)+"->"+Double.toString(y)+","+Double.toString(currheight));		
				counter++;
			}
			shapeSet.add( new XLine ( 0.0, xaxispix, width-1.0, xaxispix, "uou") );  
			if (Math.abs(minval) >= Math.abs(maxval)) maxAsbValueCoordY = xaxispix -(minval * heightScale);    
			else  maxAsbValueCoordY = xaxispix -(maxval * heightScale);    
		}

		return shapeSet;
	}

	// After all shape as been created we get the required Width
	public double get_shapeMaxX () {
		return this.shapeMaxX;
	}
	public double get_xAxisYCoord () {
		return xaxispix;
	}
	public double get_maxAsbValueCoordY() {
		return maxAsbValueCoordY;
	}
}