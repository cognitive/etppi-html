package rtcw;

import java.awt.event.*;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import java.util.Enumeration;

// jeudi 17 juillet 2003
// Interface 
//  getColors renvoie un vecteur de couleur correspondant au choix
//  setColors recoit un vecteur de couleur et met les boutons aux couleurs choisies

class NickConstraintPanel extends JPanel implements ActionListener  {

	JCheckBox onlyForNickChB=	new JCheckBox("Ignore rounds not played by", false);
	JComboBox allNickCB=		new JComboBox();

	public NickConstraintPanel() {
		super ();

		//this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.setLayout(new GridLayout(2,1));
		
		this.add( onlyForNickChB );
		onlyForNickChB.addActionListener(this);
		//resetBt.setActionCommand("reset");
		
		this.add( allNickCB);
		allNickCB.setEnabled( false );
		allNickCB.addActionListener(this);
		//applyBt.setActionCommand("apply");
		
		Enumeration<String> enumeration= Round.getDifferentPlayerNames();
		while (enumeration.hasMoreElements()) {
			allNickCB.addItem(enumeration.nextElement());
		}
	}
	
	public String getNickConstraint() {
		if (onlyForNickChB.isSelected()) { return (String)allNickCB.getSelectedItem(); }
		else return new String("");
	}
	public void setNickConstraint(String nickName) {
		if ( nickName.length() == 0) {
			onlyForNickChB.setSelected(false);
			allNickCB.setEnabled(false);
		} else {
			onlyForNickChB.setSelected(true);
			allNickCB.setEnabled(true);
			allNickCB.setSelectedItem(nickName);
		}
	}

	
	public void actionPerformed(ActionEvent e) {
	
		Object source = e.getSource();
		
		if (source instanceof JCheckBox) {
			JCheckBox cb = (JCheckBox)source;
			allNickCB.setEnabled( cb.isSelected() );
		}
	}
	
}