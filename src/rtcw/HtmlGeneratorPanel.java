package rtcw;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

class HtmlGeneratorPanel extends JPanel implements ActionListener
{
	Color valueColor = Color.blue;

	Box main =			new Box(BoxLayout.Y_AXIS);
	
	Box panel1 =		new Box(BoxLayout.Y_AXIS);
	Box panel2 =		new Box(BoxLayout.Y_AXIS);
	Box panel3 =		new Box(BoxLayout.Y_AXIS);
	Box panel4 =		new Box(BoxLayout.Y_AXIS);
	Box panel5 =		new Box(BoxLayout.Y_AXIS);

	JLabel firstLogLB =		new JLabel("First Log ");
	JLabel lastLogLB  =		new JLabel("Last Log ");
	JLabel roundPlayedLB  =	new JLabel("Round Played ");

	JLabel flogValueLB =	new JLabel("00/00/00");
	JLabel llogValueLB =	new JLabel("00/00/00");
	JLabel rPlayedValueLB =	new JLabel("0");
	
	JLabel startDLB =		new JLabel("Start Date:");
	JLabel endDateLB  =		new JLabel("End Date:");
	
	JCheckBox killdeathCB = new JCheckBox("Kill - Death",true);
	JCheckBox hshotsCB = new JCheckBox("Head Shots",false);
	JCheckBox dgeGTCB = new JCheckBox("Dmge Given - Dmge Team",false);
	JCheckBox dgeGRCB = new JCheckBox("Dmge Given - Dmge Received",false);
	
	JCheckBox bsenseCB = new JCheckBox("Battle Sense",false);
	JCheckBox lweaponCB = new JCheckBox("Light Weapon",false);
	JCheckBox hweaponCB = new JCheckBox("Heavy Weapon",false);
	JCheckBox covOpsCB = new JCheckBox("Cov Ops",false);
	JCheckBox signalsCB = new JCheckBox("Signals",false);
	JCheckBox engineeringCB = new JCheckBox("Engineering",false);
	JCheckBox firstAidCB = new JCheckBox("First Aid",false);
	
	
	ImageIcon	applyIcon = ETStat.createImageIcon("MiscIconPack/okay.png");	
	JButton generateBt= new JButton("Generate",applyIcon);

	private ETQuery query;

	private String startDate;

	private String endDate;
	
	public void setDates(String start, String end) {
		this.startDate = start;
		this.endDate = end;
		flogValueLB.setText(start);
		llogValueLB.setText(end);
	}

	public void setRoundPlayed( int rplayed ) {
		setRoundPlayed(Integer.toString(rplayed));
	}

	private void setRoundPlayed( String rplayed ) {
		rPlayedValueLB.setText(rplayed);
	}

	public HtmlGeneratorPanel ( ETQuery q) {
		this.query = q;

		Border insideSpaceBorder=	BorderFactory.createEmptyBorder(8,12,8,12);
		Border bevelLowered =		BorderFactory.createBevelBorder(BevelBorder.LOWERED);
		Border mo_title =			BorderFactory.createTitledBorder(bevelLowered, "HTML Settings");

		this.setBorder( BorderFactory.createCompoundBorder(
						  mo_title,
						  insideSpaceBorder));

		this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));

		panel1.add( firstLogLB);
		panel1.add( lastLogLB );
		panel1.add( roundPlayedLB );
		panel1.add( new JLabel(" ") );
		panel1.add(generateBt);

		killdeathCB.addActionListener(this);
		killdeathCB.setActionCommand("cb");
		hshotsCB.addActionListener(this);
		hshotsCB.setActionCommand("cb");
		dgeGRCB.addActionListener(this);
		dgeGRCB.setActionCommand("cb");
		dgeGTCB.addActionListener(this);
		dgeGTCB.setActionCommand("cb");

		bsenseCB.addActionListener(this);
		bsenseCB.setActionCommand("cb");
		lweaponCB.addActionListener(this);
		lweaponCB.setActionCommand("cb");
		hweaponCB.addActionListener(this);
		hweaponCB.setActionCommand("cb");
		covOpsCB.addActionListener(this);
		covOpsCB.setActionCommand("cb");
		engineeringCB.addActionListener(this);
		engineeringCB.setActionCommand("cb");
		signalsCB.addActionListener(this);
		signalsCB.setActionCommand("cb");
		firstAidCB.addActionListener(this);
		firstAidCB.setActionCommand("cb");
		
		

		generateBt.addActionListener(this);
		generateBt.setActionCommand("generate");
		generateBt.setEnabled(true);

		flogValueLB.setForeground( valueColor );
		llogValueLB.setForeground( valueColor );
		rPlayedValueLB.setForeground( valueColor );
		panel2.add( flogValueLB );
		panel2.add( llogValueLB );
		panel2.add( rPlayedValueLB);
		panel2.add( new JLabel(" ") );
		panel2.add( new JLabel(" ") );
		panel2.add( new JLabel(" ") );
		
		panel3.add(killdeathCB);
		panel3.add(hshotsCB);
		panel3.add(dgeGTCB);
		panel3.add(dgeGRCB);

		panel4.add(bsenseCB);
		panel4.add(lweaponCB);
		panel4.add(hweaponCB);
		panel4.add(covOpsCB);

		
		panel5.add(engineeringCB);
		panel5.add(signalsCB);
		panel5.add(firstAidCB);
		panel5.add( Box.createVerticalStrut(25));
		
		this.add(panel1);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel2);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel3);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel4);
		this.add(Box.createHorizontalStrut(10));
		this.add(panel5);
		return;
	}

	public String doubleToString ( double d ) {

		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits( 1 );
		df.setMinimumFractionDigits( 1 );
		return df.format( d );
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Vector<Integer> htmlContentParts = new Vector<Integer>();
		
		if ( "cb".equals(e.getActionCommand()) ) {
			htmlContentParts = new Vector<Integer>();
			// Determine what is checked
			if (killdeathCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.KILLDEATHDELTA));
			if (hshotsCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.HEADSHOT));
			if (dgeGTCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.DAMAGEGR));
			if (dgeGRCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.DAMAGEGT));
			
			if (bsenseCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.BSENSE));
			if (lweaponCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.LWEAPONS));
			if (hweaponCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.HWEAPONS));
			if (covOpsCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.COVOPS));
			if (engineeringCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.ENGINEERING));
			if (signalsCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.SIGNALS));
			if (firstAidCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.FIRSTAID));

		
			// Enable / Disable Generate Button if any/nothing is checked
			if (htmlContentParts.isEmpty()) generateBt.setEnabled(false);
			else generateBt.setEnabled(true);
		}

		if ( "generate".equals(e.getActionCommand()) ) {
			htmlContentParts = new Vector<Integer>();
			
			generateBt.setEnabled(false);
			// Prepare output file
			//File output = new File(key+"-index.html");
			File output = new File("index.html");
			FileWriter outputfw = null;
			BufferedWriter outputbw = null;
			try {
						outputfw  = new FileWriter( output );
						outputbw = new BufferedWriter(outputfw);
			} catch (IOException e1) {
						e1.printStackTrace();
						return;
			}
							
			// Determine what is checked
			if (killdeathCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.KILLDEATHDELTA));
			if (hshotsCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.HEADSHOT));
			if (dgeGTCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.DAMAGEGR));
			if (dgeGRCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.DAMAGEGT));
			//System.out.println("generate clicked");
			if (bsenseCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.BSENSE));
			if (lweaponCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.LWEAPONS));
			if (hweaponCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.HWEAPONS));
			if (covOpsCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.COVOPS));
			if (engineeringCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.ENGINEERING));
			if (signalsCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.SIGNALS));
			if (firstAidCB.isSelected()) htmlContentParts.add(new Integer(ETQuery.FIRSTAID));

			StringBuffer hd = new StringBuffer();
			hd.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
			hd.append("<HTML>\n");
			hd.append("<HEAD>\n");
			String mainPageTitle = new String("Enemy Territory Player Statistics for the period from "+this.startDate+" to "+this.endDate);
			hd.append("<META http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\">\n");
			hd.append("<TITLE>"+mainPageTitle+"</TITLE>\n");
			hd.append("</HEAD>\n").append("<BODY BGCOLOR=\"#000000\">\n");


			StringBuffer content = new StringBuffer();

			long genstart = System.currentTimeMillis();

			Enumeration<Integer> allParts = htmlContentParts.elements();
			while (allParts.hasMoreElements()) {
				int currentElem = ((Integer)allParts.nextElement()).intValue();
				query.set_skillDataType(Skill.SELECT_POINTS);
				query.prepareVectors( currentElem);

				DrawPanel dp = new DrawPanel( query, null); 
				String title = ETQuery.getConstantAsString(currentElem)+" "+this.startDate+" "+this.endDate;
				content.append( dp.generateHtml(title) );
				content.append( "\n<BR>\n" );
				//System.out.println("html generated with preparevector = "+title);
			}

			long genend = System.currentTimeMillis();

			System.out.println("Elapsed time for HTML Generation : "+Long.toString(genend-genstart)+"ms.");

			StringBuffer ft = new StringBuffer();
			ft.append(ETStat.getHtmlSignature());
			ft.append("\n<BR>\n");
			ft.append("</BODY>\n").append("</HTML>");

			// Write all to file			
			try {
				outputbw.write(hd.toString());
				outputbw.write(content.toString());
				outputbw.write(ft.toString());
				outputbw.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			generateBt.setEnabled(true);
		}

	}


}
