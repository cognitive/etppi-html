package rtcw;

class AccurateWeapon extends Weapon {

	int headshot = 0;

	public AccurateWeapon( String weapName, double accu, String hits, int kill, int death, int headshot ) {

		super( weapName, accu, hits, kill, death );
		this.headshot = headshot;
		//System.out.println("new weapon "+weapName+":"+Double.toString(accu)+","+hits+","+Integer.toString(kill)+","+Integer.toString(death));

	}

}