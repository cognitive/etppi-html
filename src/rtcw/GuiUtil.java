package rtcw;

import java.awt.Component;
import java.awt.Container;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.JFrame;
import javax.swing.JLabel;

class  GuiUtil {

	
	static GuiUtil _instance = null;
	
	static public GuiUtil getInstance() {
		if ( _instance == null ) _instance = new GuiUtil();
		return _instance;
	}

	public int setFrameEnabled( JFrame f,  boolean enabled) {
		return setFrameEnabled( f, enabled, false);
	}
	
	public int setFrameEnabled( JFrame f,  boolean enabled, boolean skipLabel) {
		return enableContainerWidget( f.getContentPane(), enabled, skipLabel );
	}


	private int enableContainerWidget( Container container, boolean enabled, boolean skipLabel ) {	

		Component [] comps = container.getComponents();
		int componentCounter = 0;

		//System.out.println("enableContainerWidget with skiplabel = "+skipLabel);
		
		int counter = 0;
		while (counter < comps.length )	{

			Component currentComp =  comps[counter];
			
			// Recursive call if Container
			if ( currentComp instanceof Box || currentComp instanceof JPanel || currentComp instanceof JScrollPane || currentComp instanceof JViewport )	{
				componentCounter += enableContainerWidget( (Container)currentComp, enabled, skipLabel);

			} else { // Simple setEnabled - with skipLabel available
					if (!skipLabel || !(currentComp instanceof JLabel)) {
						currentComp.setEnabled(enabled);
						componentCounter++;
					}
			}
			counter++;
		}
		
		return componentCounter;
	}

}
