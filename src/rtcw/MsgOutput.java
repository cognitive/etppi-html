package rtcw;

public class MsgOutput
{
	
	int logLevel= SEVERE;
	String callerClassName = "MsgOutput";
	
	public static int SEVERE 	= 1;
	public static int ERROR 	= 2;
	public static int WARNING 	= 3;
	public static int INFO 		= 6;
	public static int CONFIG 	= 8;
	public static int DEBUG 	= 10;
	

	public MsgOutput() {
	}

	public MsgOutput ( int level) {
	this.logLevel = level;
	}

	public MsgOutput ( int level, String callerClassName) {
		this.logLevel = level;
		this.callerClassName = callerClassName;
	}

	public void severe ( String s) {
		if (  logLevel >= SEVERE ) {
			System.out.println("SEVERE: "+this.callerClassName+s);
		}
	}
	public void info ( String s) {
		if (  logLevel >= INFO ) {
			System.out.println("INFO: "+this.callerClassName+s);
		}
	}
	public void warning ( String s) {
		if (  logLevel >= WARNING ) {
			System.out.println("WARNING: "+this.callerClassName+s);
		}
	}
	public void config ( String s) {
		if (  logLevel >= CONFIG ) {
			System.out.println("CONFIG: "+this.callerClassName+s);
		}
	}
	public void debug ( String s) {
		if (  logLevel >= DEBUG ) {
			System.out.println("DEBUG: "+this.callerClassName+s);
		}
	}
}

