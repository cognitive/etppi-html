package rtcw;

import java.awt.geom.Rectangle2D;

class XRectangle extends Rectangle2D.Double
{
	String legend = null;
	String vStr = null;
	double value = 0.0;

	public XRectangle ( double x, double y, double width, double height, String valueStr, double val, String lgd ) {
		super( x, y, width, height );
		this.legend= lgd;
		this.vStr= valueStr;
		this.value = val;
	}

	public String get_valueStr () {
		return this.vStr;
	}
	
	public double get_value () {
		return this.value;
	}
}


