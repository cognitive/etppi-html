package rtcw;

import java.net.*;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.util.Locale;


import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;

import java.awt.Dimension;
import java.io.FileOutputStream;
import java.io.DataInputStream;

class Update 
{
	static MsgOutput o= new MsgOutput( MsgOutput.INFO,"RTCW.Update" ); 
	
	static public void start() 
	{

		String osname  = System.getProperty("os.name");
		String country = Locale.getDefault().getDisplayCountry();
		String lmodified = new String("");
		URL u = null;
		
		try	{
			lmodified = Long.toString( (new File("./ETPPI.jar").lastModified()) );
		} catch (java.lang.NullPointerException e) {}



		try {

			// Sourceforge !!!
			String prefix		= new String( "http://urtaa.sourceforge.net/");
			String urlStatic	= new String("php/etppi/version/version.php");
			String values		= new String("?osname="+osname+"&country="+country+"&lmodified="+lmodified+"&version="+ETStat.getVersionMajor()+ETStat.getVersionMinor());
			String urlDynamic	= values.replace(' ','+');
			u= new URL( prefix + urlStatic + urlDynamic );

			//o.debug("URL: "+prefix + urlStatic + urlDynamic);		
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}
		
		String major = null;
		String minor = null;			
		
		try{
			o.info("Checking for more recent versions.");
			InputStream is = u.openStream();
			BufferedReader isr = new BufferedReader( new InputStreamReader(is) );
			major = isr.readLine();
			minor = isr.readLine();
			isr.close();
			is.close();
			o.info("Latest Available Version: "+major+"."+minor);
			o.info("Current Installed Version: "+ETStat.getVersionMajor()+"."+ETStat.getVersionMinor());
		}
		catch ( Exception e ) {
			o.info("Server is not reachable. Can not check for updates.");
			return;
		}
		
		// More recent version available
		if (  Integer.parseInt(major) > ETStat.getVersionMajor() || 
			 (Integer.parseInt(major) == ETStat.getVersionMajor() && Integer.parseInt(minor) > ETStat.getVersionMinor()) ) {
			downloadUpdate();
			JOptionPane.showMessageDialog(null, "Please restart ETPPI", "ETPPI Updated", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}

		o.info("Installed version is up to date. Great.");
		//System.exit(0);

	}


	static public void downloadUpdate() {
		
		JFrame f = new JFrame("Updating");
		Box p = new Box(BoxLayout.Y_AXIS); 
		JProgressBar progressBar = new JProgressBar();
		StatusBarPanel sbp = new StatusBarPanel();
		URL u = null;

		sbp.setText("Connecting...");
		progressBar.setIndeterminate( true );
		//p.add( progressBar );
		//p.add( Box.createVerticalStrut(5));
		//p.add ( sbp );
		
		f.setContentPane( p );
		f.getContentPane().add(progressBar);
		progressBar.setPreferredSize( new Dimension( (int)progressBar.getPreferredSize().getWidth(), (int)progressBar.getMinimumSize().getHeight()+2 ) );
		f.getContentPane().add(sbp);
		
		f.pack();
		f.setVisible( true );

		// Sourceforge !!!
		String prefix		= new String( "http://urtaa.sourceforge.net/");
		String urlStatic	= new String("php/etppi/dl/dljava.php");
	
		try
		{
			u= new URL( prefix + urlStatic );
			
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}
		
		DataInputStream bais = null;			
		byte [] buff = new byte[8];
		byte [] kb   = new byte[2048];
		long fileSize = 0;

		try{
			InputStream is = u.openStream();
			bais = new DataInputStream( is );
			bais.read( buff );
			fileSize = Long.parseLong( new String( buff ), 16);
			//System.out.println("byte size = "+ fileSize);

			FileOutputStream fos = new FileOutputStream( new File ("./ETPPI.jar") ); 
			sbp.setText("Downloading...");
			progressBar.setMaximum((int)fileSize/2048);
			progressBar.setValue(0);
			progressBar.setStringPainted(true);
			progressBar.setIndeterminate(false);
			p.update(p.getGraphics());

			int totalread = 0;
			int byteread = 0;

			while ( (byteread = is.read(kb)) != -1)
			{									
				fos.write( kb,0,byteread );									
				totalread += byteread;					
				progressBar.setValue((int)totalread/2048);						
			}
			//fos.flush();
			fos.close();
			bais.close();
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}

		f.setVisible( false );
	}
}