package rtcw;

///*************************************************************************
//** UrTAA - Urban Terror Accuracy Analyser
//** Author:       Denis "VatiCool" ETIENNE
//** Email:        Denis.Etienne@club-internet.fr
//** URL:          http://perso.club-internet.fr/denis.etienne/2003/urtaa/
//** Version:      1.2a
//** Last Updated: April. 02, 2003
//**
//** Copyright (C) 2003  Denis "VatiCool" ETIENNE
//**
//** This program is free software; you can redistribute it and/or
//** modify it under the terms of the GNU General Public License version 2
//** as published by the Free Software Foundation.
//**
//** This program is distributed in the hope that it will be useful,
//** but WITHOUT ANY WARRANTY; without even the implied warranty of
//** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//** GNU General Public License for more details.
//**
//** You should have received a copy of the GNU General Public License
//** along with this program; if not, write to the Free Software
//** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//******************************************************************************/

/**
 * @author Denis ETIENNE
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import java.util.regex.*;
 
public class PerlLike
{

/**
 * Method BasicExtraction locates a subString (group) in a pattern and returns it
 * @param aString the string where to locate the pattern from
 * @param aPattern the pattern to match against
 * @param groupNumber the position of the group to return
 * @return String that corresponds to the good group (subpattern), enpty String if not found
 * Note: index=0 equals whole expression - first submatch is given by index=1  
 * <PRE>for example:
 * aString: HEAD.img	= "head";
 * aPattern: ^([A-Z-]*).img[\\s\\t]*=[\\s\\t]*\"(.*)\";
 * group#1 = HEAD
 * group#2 = head</PRE>
 **/
static public String BasicExtraction ( String aString, String aPattern, int groupNumber  ) {

	Pattern p = Pattern.compile( aPattern );
	Matcher m = p.matcher( aString ); 
	String aSubString = new String();
	
	if ( m.matches() && m.groupCount() >= groupNumber) {
		aSubString = aString.substring(m.start(groupNumber),m.end(groupNumber));
	}
	return aSubString; 
}

/** Method BasicExtractionPatternProvided locates a subString (group) in a pattern and returns it
 * @param aString the string where to locate the pattern from
 * @param p the COMPILED pattern to match against
 * @param groupNumber the position of the group to return
 * @return String
 * Note: index=0 equals whole expression - first submatch is given by index=1  
 *
 */
static public String BasicExtractionCP ( String aString, Pattern aCompiledPattern, int groupNumber  ) {

	Pattern p = (Pattern)aCompiledPattern;
	Matcher m = p.matcher( aString ); 
	String aSubString = new String();
	
	if ( m.matches() && m.groupCount() >= groupNumber) {
		aSubString = aString.substring(m.start(groupNumber),m.end(groupNumber));
	}
	return aSubString; 
}


/**
 * Method BasicReplace replaces all subString in a String by a replacementString
 * @param aString the string where to locate the pattern from
 * @param aPattern the pattern to match against
 * @param replacementString the string to use for replacement
 * @return String where pattern matching substrings have all been replaced with replacementString
 **/
static public String BasicReplace ( String aString, String aPattern, String replacementString  ) {

	Pattern p = Pattern.compile( aPattern );
	Matcher m = p.matcher( aString ); 

	return m.replaceAll(replacementString);
}

static public Pattern CompilePattern ( String aPattern ) {
	return Pattern.compile( aPattern );
}

}
