package rtcw;

// Frame related
import javax.swing.JFrame;
// Layout related

// Widget related
import javax.swing.JScrollPane;
import java.awt.Dimension;

class DrawFrame extends JFrame  
{

	DrawPanel dp  =	null;
	JScrollPane sp = null;

//	public DrawFrame(String title) throws HeadlessException {
//		super ( title );
//		this.getContentPane().add(dp);
//	}

	public DrawFrame ( String title, ETQuery q, Dimension d ) {
	
		super ( title );
		dp = new DrawPanel ( q, d );
		sp = new JScrollPane( dp, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED  );
		sp.setPreferredSize(d);
		//dp.generateHtml(title);
		this.getContentPane().add(sp);
	}

}
