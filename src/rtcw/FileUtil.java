package rtcw;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;

// RTCW ET specific
import java.util.Vector;
import java.util.Enumeration;


public class FileUtil
{
	private static FileUtil	_instance = null;
	private static MsgOutput o = new MsgOutput( MsgOutput.WARNING,"RTCW.FileUtil" );

	public static FileUtil getInstance ( ) {
		if (_instance == null ) {
			_instance = new FileUtil();	
		}
		return _instance;
	}

	public boolean CheckFileRAccess ( String aFileName ) { 

		File aFile = new File(aFileName);

		if (!aFile.canRead()) {
			o.info("File "+aFileName+" can not be accessed for reading.");
			return false;
		} 
		o.info("File "+aFileName+" accessed for reading.");
		return true;
	}		

	public boolean CheckFileWAccess ( String aFileName ) { 

		File aFile = new File(aFileName);

		if (aFile.exists() && !aFile.canWrite()) {
			o.info("File "+aFileName+" can not be accessed for writing.");
			return false;
		} 
		o.info("File "+aFileName+" accessed for writing.");
		return true;
	}		

	/**
	 * Method GetReader.
	 * @param aFileName
	 * @return BufferedReader on succes and null on failure
	 */
	public BufferedReader GetReader ( String aFileName ) {

		BufferedReader reader = null; 
		try {
			reader = new BufferedReader(new FileReader(aFileName));
		} catch ( Exception e ) {
			 e.printStackTrace(); 
			o.severe("Can't get a buffered reader from "+aFileName+".");
			return reader;
		}
		o.config("Got buffered reader from "+aFileName+".");
		return reader;
	}


	/**
	 * Method GetWriter.
	 * @param aFileName
	 * @return BufferedWriter on succes and null on failure
	 */
	public BufferedWriter GetWriter ( String aFileName ) {

		BufferedWriter writer = null; 
		try {
			writer = new BufferedWriter(new FileWriter(aFileName));
		} catch ( Exception e ) {
			 e.printStackTrace(); 
			o.severe("Can't get a buffered writer to "+aFileName+".");
			return writer;
		}
		o.config("Got buffered writer to "+aFileName+".");
		return writer;
	}

	public int writeLine(BufferedWriter bw, String line) {
		try {
			bw.write(line);
			bw.newLine();
		} catch ( java.io.IOException e ) {
			e.printStackTrace(); 
			o.severe("Write to file failed"); 
			return -1;
		}
		return 	line.length();	
	}

	public String readLine (BufferedReader br) {
	
		String currLine = null; 
		
		try {
				currLine = br.readLine();
		} catch ( IOException e ) {
			e.printStackTrace(); 
			o.severe("Read file failed"); 
		}
		
		if ( currLine == null ) {
			o.info("Readline on "+br.toString()+" reached end of file."); 
		}
		return currLine;		
	}


	/**
	 * Method CloseReader.
	 * @param bf the buffered Reader to close
	 * @return boolean true on success
	 */
	public boolean CloseReader ( Reader bf ) {
		boolean ret = false;
		try {
			bf.close();
			ret = true;
		} catch ( Exception e ) {
			e.printStackTrace(); 
			o.severe("Can't close a file reader from "+bf.toString());
		}
		o.config("File reader closed "+bf.toString()+".");
		return ret;
	}

	/**
	 * Method CloseWriter.
	 * @param bw the buffered Writer to close
	 * @return boolean true on success
	 */
	public boolean CloseWriter ( Writer bw ) {
		boolean ret = false;
		try {
			bw.close();
			ret = true;
		} catch ( Exception e ) {
			e.printStackTrace(); 
			o.severe("Can't close a file writer to "+bw.toString());
		}
		o.config("File writer closed "+bw.toString()+".");
		return ret;
	}

	// RTCW ET specific

	
	public Vector<String> getSettings (  String fname, String section ) {
		Vector<String> result = new Vector<String>();
		
		if ( !CheckFileRAccess ( fname ) ) { return result; }

		BufferedReader reader = GetReader( fname ) ;
		if ( reader == null ) {	return result; }

		try
		{
			String line = reader.readLine();

			// look for section
			while (line != null && line.compareTo (section) != 0) {
				line = reader.readLine();
			}
			
			// Section not found
			if (line == null) {
				return result;
			}
			
			// Section found - add until EOF or new SECTION
			line = reader.readLine();
			while ( line != null && line.charAt(0) != '@' )	{
				result.add ( line );
				line = reader.readLine();
			}
	
		}
		catch ( java.io.IOException exception ) {
			exception.printStackTrace();
			result = new Vector<String>();
			return result;
		}

		if (!CloseReader(reader)){
			result = new Vector<String>();
		}
		return result;
	}


	// Used to store a vector of String in a given file 
	// Must be changed in the future
	public boolean saveSettings ( String fname, String section, Vector<String> data ) {
		
		if ( !CheckFileWAccess ( fname ) ) { return false; }

		BufferedWriter writer = GetWriter( fname ) ;
		if ( writer == null ) {	return false; }

		try
		{
			writer.write( section, 0, section.length() ) ; 
			writer.newLine();

			for (Enumeration<String> e = data.elements() ; e.hasMoreElements() ;) {
			
				String line = (String)e.nextElement();
				writer.write( line, 0, line.length() ) ; 
				writer.newLine();
			}
		}
		catch ( java.io.IOException exception ) {
			exception.printStackTrace();
			return false;
		}

		if (!CloseWriter(writer)){
			return false;
		}
		return true;	
	}

	public boolean checkFiles ( Vector<String> data ) {

		for (Enumeration<String> e = data.elements() ; e.hasMoreElements() ;) {
			if ( !CheckFileRAccess ( (String)e.nextElement() ) ) {
				return false;
			}
		}
		return true;
	}

	public Vector<String> getFileVectorFromDirVector ( Vector<File> dirVector ) {

		Vector<String> fVector = new Vector<String>();

		int idx = 0;
		while (idx < dirVector.size()) {
		
			File currDir = (File)dirVector.elementAt(idx);
			File [] allFiles = currDir.listFiles();
			
			int subidx=0;
			while ( subidx < allFiles.length ) {
				fVector.add( allFiles[subidx].getAbsolutePath());
				subidx++;
			}
			idx++;

		}
		return fVector;
	}

}


